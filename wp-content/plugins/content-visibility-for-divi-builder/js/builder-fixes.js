(function($) {
	'use strict';
	
	$( document ).ready( function() {
		var OriginalPerformSaving = ET_PageBuilder.ModalView.prototype.performSaving;
		ET_PageBuilder.ModalView = ET_PageBuilder.ModalView.extend({
			performSaving : function( option_tabs_selector ) {
				var attributes = {},
					options_selector = typeof option_tabs_selector !== 'undefined' && '' !== option_tabs_selector ? option_tabs_selector : 'input, select, textarea, #et_pb_content_main';
				var $et_form_validation;
				$et_form_validation = $(this)[0].$el.find('form.validate');
				if ( $et_form_validation.length ) {
					validator = $et_form_validation.validate();
					if ( !validator.form() ) {
						et_builder_debug_message('failed form validation');
						et_builder_debug_message('failed elements: ');
						et_builder_debug_message( validator.errorList );
						validator.focusInvalid();
						return;
					}
					et_builder_debug_message('passed form validation');
				}
				this.$( options_selector ).each( function() {
					var $this_el = $(this),
						name = $this_el.is('#et_pb_content_main') ? 'et_pb_content_new' : $this_el.attr('id');
					
					// name attribute is used in normal html checkboxes, use it instead of ID
					if ( $this_el.is( ':checkbox' ) ) {
						name = $this_el.attr('name');
					}
					
					if ( typeof name === 'undefined' || ( -1 !== name.indexOf( 'qt_' ) && 'button' === $this_el.attr( 'type' ) ) ) {
						// settings should have an ID and shouldn't be a Quick Tag button from the tinyMCE in order to be saved
						return true;
					}
					
					if ( $this_el.hasClass( 'et-pb-helper-field' ) ) {
						// don't process helper fields
						return true;
					}
					
					if ( ( !$this_el.is( ':checkbox' ) || typeof attributes[name] !== 'undefined' ) && $this_el.is( '#et_pb_cvdb_content_visibility_check' ) ) {
						// Process content visibility check
						
						// Replace temporary ^~ signs with right brackets
						$this_el.val( $this_el.val().replace( /\]/g, '%5D' ) );
					}
				});
				return OriginalPerformSaving.call(this);
			}
		});
		
		var OriginalRender = ET_PageBuilder.ModuleSettingsView.prototype.render;
		ET_PageBuilder.ModuleSettingsView = ET_PageBuilder.ModuleSettingsView.extend({
			render : function() {
				var $icon_font_options = [ "et_pb_font_icon", "et_pb_button_one_icon", "et_pb_button_two_icon", "et_pb_button_icon" ];
				// Replace encoded double quotes with normal quotes,
				// escaping is applied in modules templates
				_.each( this.model.attributes, function( value, key, list ) {
					if ( typeof value === 'string' && key !== 'et_pb_content_new' && -1 === $.inArray( key, $icon_font_options ) && ! /^\%\%\d+D\%\%$/.test( $.trim( value ) ) ) {
						return list[ key ] = value.replace( /%5D/g, ']' );
					}
				} );
				return OriginalRender.call(this);
			}
		});
		/*
		var OriginalGenerateContent = ET_PageBuilder.AdvancedModuleSettingsView.prototype.generateContent;
		var OriginalGenerateAdvancedSortableItems = ET_PageBuilder.AdvancedModuleSettingsView.prototype.generateAdvancedSortableItems;
		ET_PageBuilder.AdvancedModuleSettingsView = ET_PageBuilder.AdvancedModuleSettingsView.extend({
			generateContent : function() {
				var result = OriginalGenerateContent.call(this);
				this.$content_textarea.html( this.$content_textarea.html().replace( /%5D/g, '^~' ) );
				return result;
			},
			generateAdvancedSortableItems : function( content, module_type ) {
				var et_pb_shortcodes_tags = ET_PageBuilder_App.getShortCodeChildTags(),
					reg_exp = window.wp.shortcode.regexp( et_pb_shortcodes_tags ),
					inner_reg_exp = ET_PageBuilder_App.wp_regexp_not_global( et_pb_shortcodes_tags );
					content = content.replace( et_pb_shortcodes_tags, function(outer_match) {
						var i = 0;
						return outer_match.replace( inner_reg_exp, function( inner_match ) {
							if ( i != 3 ) {
								i++;
								return inner_match;
							} else {
								var result = '',
									shortcode_attributes = inner_match !== ''
										? window.wp.shortcode.attrs( inner_match )
										: '';
								if ( _.isObject( shortcode_attributes['named'] ) ) {
									for ( var key in shortcode_attributes['named'] ) {
										// Replace temporary ^~ signs with right brackets
										result += key + "='" + shortcode_attributes['named'][key].replace( /%5D/g, ']' ) + "' ";
									}
									if ( _.isArray( shortcode_attributes['numeric'] ) ) {
										for ( var j = 0; j < shortcode_attributes['numeric'].length; j++ ) {
											result += shortcode_attributes['numeric'][j] + ' ';
										}
									}
									return result;
								} else {
									return inner_match;
								}
							}
						} );
					} );
				return OriginalGenerateAdvancedSortableItems.call( this, content, module_type );
			}
		});
		
		var OriginalGenerateModuleShortcode = ET_PageBuilder.AppView.prototype.generateModuleShortcode;
		ET_PageBuilder.AppView = ET_PageBuilder.AppView.extend({
			generateModuleShortcode : function( $module, open_tag_only, layout_type, ignore_global_tag, defined_module_type, ignore_global_tabs ) {
				var module_settings;
				module_settings = module.attributes;
				for ( var key in module_settings ) {
					if ( typeof ignore_global_tag === 'undefined' || 'ignore_global' !== ignore_global_tag || ( typeof ignore_global_tag !== 'undefined' && 'ignore_global' === ignore_global_tag && 'et_pb_global_module' !== key && 'et_pb_global_parent' !== key ) ) {
						if ( typeof ignore_global_tabs === 'undefined' || 'ignore_global_tabs' !== ignore_global_tabs || ( typeof ignore_global_tabs !== 'undefined' && 'ignore_global_tabs' === ignore_global_tabs && 'et_pb_saved_tabs' !== key ) ) {
							var setting_name = key,
								setting_value;
							
							if ( setting_name.indexOf( 'et_pb_' ) === -1 && setting_name !== 'admin_label' ) continue;
							
							setting_value = typeof( module.get( setting_name ) ) !== 'undefined' ? module.get( setting_name ) : '';
							
							if ( setting_name !== 'et_pb_content_new' && setting_name !== 'et_pb_raw_content' && setting_value !== '' &&  typeof setting_value === 'string' ) {
								module.set( setting_name, setting_value.replace( /\]/g, '%5D' ) );
							}
						}
					}
				}
				return OriginalGenerateModuleShortcode.call(this);
			}
		});
		*/
	});
})(jQuery);
