<?php
/**
 * The template for displaying all single posts.
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 * @package Emotions Lite
 */

$singlestyle = esc_attr(get_theme_mod( 'single_style', 'single1' ));
get_header(); ?>


<div id="primary" class="content-area">
		<main id="main" class="site-main <?php echo $singlestyle ?>" role="main" itemprop="mainContentOfPage">


					<?php
						$single_style = esc_attr(get_theme_mod( 'single_style', 'single1' ) );                          
						switch ($single_style) {

							// Single with a right sidebar column
							case "single1" : 
								echo '<div class="row"><div class="medium-8 columns">';   
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', 'single' );									
										if ( comments_open() || get_comments_number() ) {
										comments_template();
										}
									endwhile;
								echo '</div><div class="medium-4 columns">';
									get_sidebar( 'right' );
								echo '</div></div>';					
							break;

							// Single with leftt sidebar column
							case "single2" : 
								echo '<div class="row"><div class="medium-8 medium-push-4 columns">';   
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', 'single' );
										if ( comments_open() || get_comments_number() ) {
										comments_template();
										}
									endwhile;
								echo '</div><div class="medium-4 medium-pull-8 columns">';
									get_sidebar( 'left' );
								echo '</div></div>';					
							break;
							
							// Single without a left or right sidebar
							case "single3" : 
							echo '<div class="column row">';
								while ( have_posts() ) : the_post();
									get_template_part( 'template-parts/content', 'single' );
										if ( comments_open() || get_comments_number() ) {
										comments_template();
										}
									endwhile;
								echo '</div>';	
							break;
							
						}	
					?>		


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
