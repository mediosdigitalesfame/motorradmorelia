<?php
/**
 * Singleton class for handling the theme's customizer integration.
 * Special thanks to Justin Tadlock for this - https://github.com/justintadlock/trt-customizer-pro
 * License: GNU General Public License v2 or later
 * @since  1.0.0
 * @access public
 */
final class Emotions_Lite_Setup {

	/**
	 * Returns the instance.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return object
	 */
	public static function get_instance() {

		static $instance = null;

		if ( is_null( $instance ) ) {
			$instance = new self;
			$instance->setup_actions();
		}

		return $instance;
	}

	/**
	 * Constructor method.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Sets up initial actions.
	 *
	 * @since  1.0.0
	 * @access private
	 * @return void
	 */
	private function setup_actions() {

		// Register panels, sections, settings, controls, and partials.
		add_action( 'customize_register', array( $this, 'sections' ) );

		// Register scripts and styles for the controls.
		add_action( 'customize_controls_enqueue_scripts', array( $this, 'enqueue_control_scripts' ), 0 );
	}

	/**
	 * Sets up the customizer sections.
	 *
	 * @since  1.0.0
	 * @access public
	 * @param  object  $manager
	 * @return void
	 */
	public function sections( $manager ) {

		// Load custom sections.
		require get_template_directory() . '/emotions-lite-setup/section-setup.php';

		// Register custom section types.
		$manager->register_section_type( 'Emotions_Lite_Customize_Section_Setup' );

		// Register sections.
		$manager->add_section(
			new Emotions_Lite_Customize_Section_Setup(
				$manager,
				'emotions_lite',
				array(
					'title'    => esc_html__( 'Setup Tutorials ', 'emotions-lite' ),
					'setup_text' => esc_html__( 'Get Started', 'emotions-lite' ),
					'priority' => 15,
					'setup_url'  => 'https://www.shapedpixels.com/setup-emotions-lite/'
				)
			)
		);
	}

	/**
	 * Loads theme customizer CSS.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function enqueue_control_scripts() {

		wp_enqueue_script( 'emotions-lite-customize-controls', trailingslashit( get_template_directory_uri() ) . 'emotions-lite-setup/customize-controls.js', array( 'customize-controls' ) );

		wp_enqueue_style( 'emotions-lite-customize-controls', trailingslashit( get_template_directory_uri() ) . 'emotions-lite-setup/customize-controls.css' );
	}
}

// Doing this customizer thang!
Emotions_Lite_Setup::get_instance();
