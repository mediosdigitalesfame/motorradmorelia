<?php
/**
 * Pro customizer section.
 * Special thanks to Justin Tadlock for this - https://github.com/justintadlock/trt-customizer-pro
 * License: GNU General Public License v2 or later
 * @since  1.0.0
 * @access public
 */
class Emotions_Lite_Customize_Section_Setup extends WP_Customize_Section {

	/**
	 * The type of customize section being rendered.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $type = 'setup-tutorials';

	/**
	 * Custom button text to output.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $setup_text = 'Get Started';

	/**
	 * Custom pro button URL.
	 *
	 * @since  1.0.0
	 * @access public
	 * @var    string
	 */
	public $setup_url = 'https://www.shapedpixels.com/setup-emotions-lite/';

	/**
	 * Add custom parameters to pass to the JS via JSON.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function json() {
		$json = parent::json();

		$json['setup_text'] = $this->setup_text;
		$json['setup_url']  = esc_url( $this->setup_url );

		return $json;
	}

	/**
	 * Outputs the Underscore.js template.
	 *
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	protected function render_template() { ?>

		<li id="accordion-section-{{ data.id }}" class="accordion-section control-section control-section-{{ data.type }} cannot-expand">

			<h3 class="accordion-section-title">
				{{ data.title }}

				<# if ( data.setup_text && data.setup_url ) { #>
					<a href="{{ data.setup_url }}" class="button button-secondary alignright" target="_blank">{{ data.setup_text }}</a>
				<# } #>
			</h3>
		</li>
	<?php }
}
