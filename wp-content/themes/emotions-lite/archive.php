<?php
/**
 * The template for displaying archive pages.
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package Emotions Lite
 */
 
$blogstyle = esc_attr(get_theme_mod( 'blog_style', 'blog1' )); 

get_header(); ?>



<div id="primary" class="content-area">
	<div class="column row">
		<main id="main" class="site-main <?php echo $blogstyle ?>" role="main" itemprop="mainContentOfPage">

			<?php get_template_part( 'template-parts/'. $blogstyle );	?>
			
		</main>
	</div>
	
</div>

<?php 
get_footer();
