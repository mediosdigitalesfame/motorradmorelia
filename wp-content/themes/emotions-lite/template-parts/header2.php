<?php
/**
 * Header Style 2
 * This style represents an option from the customizer
 * @package Emotions Lite
 */
 ?>
 
 <header id="site-header2">
	 
	 
 
  <div class="expanded row">
    <div id="site-branding" class="columns medium-12 large-5">

   
				<span class="site-logo" itemscope itemtype="http://schema.org/Organization">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url">
						<?php emotions_lite_custom_logo(); ?>
					</a>    
				</span>  

	
		<?php  if( esc_attr(get_theme_mod( 'show_site_title', 1 ) ) ) :  ?> 
			<span id="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
		<?php endif; ?>

	<?php  if ( esc_attr(get_theme_mod( 'show_description', 1 ) ) ) :
		$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
		<span id="site-description"><?php echo $description; ?></span>
	<?php 
		endif;
	endif; ?>	
	</div>
    <div class="columns medium-12 large-7">
	<nav id="site-navigation" class="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
		<div class="toggle-container visible-xs visible-sm hidden-md hidden-lg">
				<button class="menu-toggle"><?php esc_html_e( 'Menu', 'emotions-lite' ); ?></button>
		</div>
					  
	  <?php if ( has_nav_menu( 'primary' ) ) {																			
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); 
				} else {
				wp_nav_menu( array( 'container' => '', 'menu_class' => '', 'title_li' => '' ));							
			   } 
			?>                    
	</nav>	
	</div>
	</div>
   
</header>

<div id="banner-wrapper"><?php get_sidebar( 'banner' ); ?></div>