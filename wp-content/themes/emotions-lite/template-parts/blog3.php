<?php
/*
 * Blog style 3 - Wide without sidebars
 * @package Emotions Lite
 */
 ?>

 
<div class="row">
	<div class="medium-centered columns text-center">

		<?php
		if ( have_posts() ) :
			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php elseif (is_archive()): ?>
				<header class="page-header">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="category-description">', '</div>' );
					?>
				</header>			
			<?php	endif; ?>
			<?php 	/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>			
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<header class="entry-header">
						
					<?php emotions_lite_post_title();	 ?>
				
					<?php  if( esc_attr(get_theme_mod( 'show_summary_meta', 1 ) ) ) :  ?>
							<div class="entry-meta">
								<?php emotions_lite_posted_on(); ?>
							</div>
					<?php endif; ?>
						
				</header>
				
				<?php // Check for featured image
				if ( has_post_thumbnail() ) {        
					echo '<a class="featured-image-link" href="' . esc_url( get_permalink() ) . '" aria-hidden="true">';
					the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ), 'itemprop' => "image"));
					echo '</a>';
				}
				?>
				
				<div class="entry-content">
					<?php // lets use an excerpt for blog summaries
						the_excerpt();			
					?>
					<?php	emotions_lite_multipage_nav();	?>
				</div>
			</article>	
					
		<?php 	endwhile;
			emotions_lite_blog_pagination();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>

</div>
</div>