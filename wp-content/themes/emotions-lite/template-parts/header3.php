<?php
/**
 * Header Style 3
 * This style represents an option from the customizer
 * @package Emotions Lite
 */
 
 ?>
 
 <header id="site-header3">
 
<div id="banner-wrapper">	 

<?php get_sidebar( 'banner' ); ?>	 
 
 <div id="site-branding">
 
   
		<div class="site-logo" itemscope itemtype="http://schema.org/Organization">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" itemprop="url">
				<?php emotions_lite_custom_logo(); ?>
			</a>    
		</div>  

 
 <?php  if( esc_attr(get_theme_mod( 'show_site_title', 1 ) ) ) :  ?> 
	<div id="site-title">		
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>			
	</div>
	<?php endif; ?>
	
	<?php  if ( esc_attr(get_theme_mod( 'show_description', 1 ) ) ) :
		$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
		<div id="site-description" class="hide-for-small-only"><?php echo $description; ?></div>
	<?php 
		endif;
	endif; ?>
	
	<nav id="site-navigation" class="main-navigation" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement">
		<div class="toggle-container visible-xs visible-sm hidden-md hidden-lg">
				<button class="menu-toggle"><?php esc_html_e( 'Menu', 'emotions-lite' ); ?></button>
		</div>
					  
	  <?php if ( has_nav_menu( 'primary' ) ) {																			
				wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); 
				} else {
				wp_nav_menu( array( 'container' => '', 'menu_class' => '', 'title_li' => '' ));							
			   } 
			?>                    
	</nav>
</div>
	
 </div>  
</header>

