<?php
/*
 * Blog style 2 - Top featured image with left sidebar
 * @package Emotions Lite
 */
 ?>
 
 
<div class="row">
	<div class="medium-8 medium-push-4 columns">

		<?php
		if ( have_posts() ) :
			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>
			<?php elseif (is_archive()): ?>
				<header class="page-header">
					<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="category-description">', '</div>' );
					?>
				</header>			
			<?php	endif; ?>
			<?php 	/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>			
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<header class="entry-header">
						
					<?php emotions_lite_post_title();	 ?>
				
					<?php  if( esc_attr(get_theme_mod( 'show_summary_meta', 1 ) ) ) :  ?>
							<div class="entry-meta">
								<?php emotions_lite_posted_on(); ?>
							</div>
					<?php endif; ?>
						
				</header>
				
				<?php // Check for featured image
				if ( has_post_thumbnail() ) {        
					echo '<a class="featured-image-link" href="' . esc_url( get_permalink() ) . '" aria-hidden="true">';
					the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ), 'itemprop' => "image"));
					echo '</a>';
				}
				?>
				
				<div class="entry-content">
				
				<?php // lets use an excerpt for blog summaries
				if ( esc_attr(get_theme_mod( 'use_excerpt', 0 ))) : 
					echo '<p>' . the_excerpt() . '</p>' ;
				else :
					the_content( sprintf(
						/* translators: %s: Name of current post. */
						__( 'Read More %s', 'emotions-lite' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					) );					
				endif;
				?>
					<?php	emotions_lite_multipage_nav();	?>
				</div>
			</article>	
					
		<?php 	endwhile;
			emotions_lite_blog_pagination();
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>

	</div>

	<div class="medium-4 medium-pull-8 columns">        
		<?php get_sidebar( 'left' ); ?>       
	</div>

</div>


	
	
	
	
	
	
	
	
	



