<?php
/**
 * Template part for displaying single posts.
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package Emotions Lite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		 
	<header class="entry-header">
	
	                <?php $title = get_the_title(); ?>
                    <?php if($title == '') { ?>
                    <h1 class="entry-title" itemprop="headline"><?php _e('Post ID: ', 'emotions-lite'); echo get_the_ID(); ?></h1>
                    <?php } else { ?>
                    <h1 class="entry-title" itemprop="headline"><?php the_title(); ?></h1>
                    <?php } ?>
	

					<?php  if( esc_attr(get_theme_mod( 'show_summary_meta', 1 ) ) ) :  ?>						
							<div class="entry-meta">
								<span class="line"><?php emotions_lite_posted_on(); ?></span>
							</div>
					<?php endif; ?>
					
	</header><!-- .entry-header -->

		<?php if( esc_attr(get_theme_mod( 'show_single_featured', 1 ) ) ) :          
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => the_title_attribute( 'echo=0' ), 'itemprop' => "image"));
         endif; ?>
		 
	<div class="entry-content" itemprop="text">
		<?php the_content(); ?>
		<?php	emotions_lite_multipage_nav();	?>
	</div><!-- .entry-content -->


			
	<?php if( esc_attr(get_theme_mod( 'show_single_footer', 1 ) ) ) : ?>
		<footer class="entry-footer" itemscope itemtype="http://schema.org/WPFooter">		
			<?php 
				// get the post footer info
				emotions_lite_entry_footer(); 
			?>			
		</footer>	
	<?php endif; ?>
	
		<?php	// show the author bio
			if( esc_attr(get_theme_mod( 'show_authorbio', 1 ) ) ) {
				if ( is_single() && get_the_author_meta( 'description' ) ) :
					get_template_part( 'author-bio' );
				endif;
			}	
			?>	
			
		<?php	// get the post next and previous post navigation
			if( esc_attr(get_theme_mod( 'show_postnav', 1 ) ) ) :			
				emotions_lite_post_pagination();	
			endif; 
			?>
	
</article><!-- #post-## -->