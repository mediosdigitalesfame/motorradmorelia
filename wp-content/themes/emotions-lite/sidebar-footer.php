<?php

/**
 * Footer sidebar at the bottom of the page 
 * @package Emotions Lite
 * 
 */

if (   ! is_active_sidebar( 'footer'  )	)
		return;
	// If we get this far, we have widgets. Let do this.
?>

<aside id="footer-group" class="widget-area">
	<div class="column row">
		
			 <?php dynamic_sidebar( 'footer' ); ?>
		
	</div>
</aside>
