<?php
/**
 * For displaying banners and sliders
 * @package Emotions Lite
 */
 
if ( ! is_active_sidebar( 'banner' ) ) {
	return;
}
?>

<div id="banner" class="widget-area" role="complementary">
	<?php dynamic_sidebar( 'banner' ); ?>
</div>