<?php
/**
 * Custom functions that act independently of the theme templates.
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Emotions Lite
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function emotions_lite_body_classes( $classes ) {
	// Adds a class of custom-background-image to sites with a custom background image.
	if ( get_background_image() ) {
		$classes[] = 'custom-background-image';
	}
	
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'emotions_lite_body_classes' );


/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 * @since Emotions Lite 1.0
 * Special thanks to the TwentySixteen theme
 */
function emotions_lite_widget_tag_cloud_args( $args ) {
	$args['largest'] = 0.813;
	$args['smallest'] = 0.813;
	$args['unit'] = 'rem';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'emotions_lite_widget_tag_cloud_args' );


/**
 * Move the More Link outside from the contents last summary paragraph tag.
 * @since Emotions Lite 1.0
 */
if ( ! function_exists( 'emotions_lite_move_more_link' ) ) :
	function emotions_lite_move_more_link($link) {
			$link = '<p class="more-link-wrapper">'.$link.'</p>';
			return $link;
		}
	add_filter('the_content_more_link', 'emotions_lite_move_more_link');
endif;

/**
 * Filter the except length to the users option setting.
 * @since Emotions Lite 1.0
 */
function emotions_lite_custom_excerpt_length( $length ) {
	$excerpt_length = esc_attr(get_theme_mod( 'excerpt_length', '50' ));
    return $excerpt_length;
}
add_filter( 'excerpt_length', 'emotions_lite_custom_excerpt_length', 999 );

/**
 * Filter the "more link" excerpt string link to the post.
 * Add a more link to the excerpt.
 * @since Emotions Lite 1.0
 */
function emotions_lite_excerpt_more( $more ) {
    return sprintf( '&hellip;<p class="more-link-wrapper"><a class="more-link" href="%1$s">%2$s</a></p>',
        get_permalink( get_the_ID() ),
        esc_html__( 'Read More', 'emotions-lite' )
    );
}
add_filter( 'excerpt_more', 'emotions_lite_excerpt_more' );

/**
 * Prevent page scroll after clicking read more to load the full post.
 * @since Emotions Lite 1.0 
 */
if ( ! function_exists( 'emotions_lite_remove_more_link_scroll' ) ) : 
	function emotions_lite_remove_more_link_scroll( $link ) {
		$link = preg_replace( '|#more-[0-9]+|', '', $link );
		return $link;
		}
	add_filter( 'the_content_more_link', 'emotions_lite_remove_more_link_scroll' );
endif;
	
/**
 * Custom comments style
 * @since Emotions Lite 1.0
 */

if (!function_exists('emotions_lite_comment')) {
function emotions_lite_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>

<li id="comment-<?php comment_ID() ?>">            
	<div class="comment-wrapper">
		<?php echo get_avatar($comment, 60); ?>
			<div class="comment-info">	<?php if ( in_array( 'bypostauthor', get_comment_class() ) ) : ?><span class="bypostauthor"><?php esc_html_e('Post Author', 'emotions-lite'); ?></span><?php endif; ?>				                
				<cite class="fn"><?php echo get_comment_author_link(); ?></cite>                        		
				<div class="comment-meta"><span class="comment-date"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ) ?>"><?php echo get_comment_date() . ' at ' . get_comment_time() ?></a>	</span>
				<span class="comment-edit"><?php edit_comment_link( esc_html__( 'Edit Comment', 'emotions-lite' ), '', '' ); ?></span>	
				<span class="comment-reply"><?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']) ) ); ?></span></div>
			</div>
	
		<div id="comment" class="comment">
			<?php comment_text(); ?>
		</div>
	</div>

<?php if ($comment->comment_approved == '0') : ?>
<p><em><?php esc_html_e('Your comment is awaiting moderation.', 'emotions-lite'); ?></em></p>
<?php endif; ?>
<?php 
}
}	

