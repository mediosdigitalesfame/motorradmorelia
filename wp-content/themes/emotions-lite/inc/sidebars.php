<?php 

/**
 * Register theme sidebars
 * @package Emotions Lite 
 */

 
function emotions_lite_widgets_init() {

	register_sidebar( array(
		'name' => __( 'Blog Right Sidebar', 'emotions-lite' ),
		'id' => 'blogright',
		'description' => __( 'Right sidebar for the blog', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Blog Left Sidebar', 'emotions-lite' ),
		'id' => 'blogleft',
		'description' => __( 'Left sidebar for the blog', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
	register_sidebar( array(
		'name' => __( 'Page Right Sidebar', 'emotions-lite' ),
		'id' => 'pageright',
		'description' => __( 'Right sidebar for pages', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	
	register_sidebar( array(
		'name' => __( 'Page Left Sidebar', 'emotions-lite' ),
		'id' => 'pageleft',
		'description' => __( 'Left sidebar for pages', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );	

	register_sidebar( array(
		'name' => __( 'Banner', 'emotions-lite' ),
		'id' => 'banner',
		'description' => __( 'For Images and Sliders.', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h1 class="widget-title">',
		'after_title' => '</h1>',
	) );
	
	register_sidebar( array(
		'name' => __( 'Content Top 1', 'emotions-lite' ),
		'id' => 'ctop1',
		'description' => __( 'Content Top 1 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Top 2', 'emotions-lite' ),
		'id' => 'ctop2',
		'description' => __( 'Content Top 2 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Top 3', 'emotions-lite' ),
		'id' => 'ctop3',
		'description' => __( 'Content Top 3 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Top 4', 'emotions-lite' ),
		'id' => 'ctop4',
		'description' => __( 'Content Top 4 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );		
			
	register_sidebar( array(
		'name' => __( 'Content Bottom 1', 'emotions-lite' ),
		'id' => 'cbottom1',
		'description' => __( 'Content Bottom 1 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Bottom 2', 'emotions-lite' ),
		'id' => 'cbottom2',
		'description' => __( 'Content Bottom 2 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Bottom 3', 'emotions-lite' ),
		'id' => 'cbottom3',
		'description' => __( 'Content Bottom 3 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Content Bottom 4', 'emotions-lite' ),
		'id' => 'cbottom4',
		'description' => __( 'Content Bottom 4 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	register_sidebar( array(
		'name' => __( 'Bottom 1', 'emotions-lite' ),
		'id' => 'bottom1',
		'description' => __( 'Bottom 1 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Bottom 2', 'emotions-lite' ),
		'id' => 'bottom2',
		'description' => __( 'Bottom 2 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Bottom 3', 'emotions-lite' ),
		'id' => 'bottom3',
		'description' => __( 'Bottom 3 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	register_sidebar( array(
		'name' => __( 'Bottom 4', 'emotions-lite' ),
		'id' => 'bottom4',
		'description' => __( 'Bottom 4 position', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );		

	register_sidebar( array(
		'name' => __( 'Footer', 'emotions-lite' ),
		'id' => 'footer',
		'description' => __( 'This is a sidebar position that sits above the footer menu and copyright', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="widget-title">',
		'after_title' => '</h5>',
	) );
	register_sidebar( array(
		'name' => __( 'Breadcrumbs', 'emotions-lite' ),
		'id' => 'breadcrumbs',
		'description' => __( 'For adding breadcrumb navigation if using a plugin, or you can add content here.', 'emotions-lite' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
}
add_action( 'widgets_init', 'emotions_lite_widgets_init' );

/**
 * Count the number of widgets to enable resizable widgets
 * in the content top group.
 */

function emotions_lite_ctop() {
	$count = 0;
	if ( is_active_sidebar( 'ctop1' ) )
		$count++;
	if ( is_active_sidebar( 'ctop2' ) )
		$count++;
	if ( is_active_sidebar( 'ctop3' ) )
		$count++;		
	if ( is_active_sidebar( 'ctop4' ) )
		$count++;
	$class = '';
	switch ( $count ) {
		case '1':
			$class = 'medium-12 columns';
			break;
		case '2':
			$class = 'large-6 columns';
			break;
		case '3':
			$class = 'small-12 medium-4 columns';
			break;
		case '4':
			$class = 'medium-6 large-3 columns';
			break;
	}
	if ( $class )
		echo 'class="' . $class . '"';
}

/**
 * Count the number of widgets to enable resizable widgets
 * in the content bottom group.
 */

function emotions_lite_cbottom() {
	$count = 0;
	if ( is_active_sidebar( 'cbottom1' ) )
		$count++;
	if ( is_active_sidebar( 'cbottom2' ) )
		$count++;
	if ( is_active_sidebar( 'cbottom3' ) )
		$count++;		
	if ( is_active_sidebar( 'cbottom4' ) )
		$count++;
	$class = '';
	switch ( $count ) {
		case '1':
			$class = 'medium-12 columns';
			break;
		case '2':
			$class = 'large-6 columns';
			break;
		case '3':
			$class = 'small-12 medium-4 columns';
			break;
		case '4':
			$class = 'medium-6 large-3 columns';
			break;
	}
	if ( $class )
		echo 'class="' . $class . '"';
}
/**
 * Count the number of widgets to enable resizable widgets
 * in the bottom group.
 */

function emotions_lite_bottom() {
	$count = 0;
	if ( is_active_sidebar( 'bottom1' ) )
		$count++;
	if ( is_active_sidebar( 'bottom2' ) )
		$count++;
	if ( is_active_sidebar( 'bottom3' ) )
		$count++;		
	if ( is_active_sidebar( 'bottom4' ) )
		$count++;
	$class = '';
	switch ( $count ) {
		case '1':
			$class = 'medium-12 columns';
			break;
		case '2':
			$class = 'large-6 columns';
			break;
		case '3':
			$class = 'small-12 medium-4 columns';
			break;
		case '4':
			$class = 'medium-6 large-3 columns';
			break;
	}
	if ( $class )
		echo 'class="' . $class . '"';
}
