<?php
/**
 * Custom template tags for this theme.
 * @package Emotions Lite
 */

 /**
 * Post Titles
 * Without title or with title
 */	
if ( ! function_exists('emotions_lite_post_title') ) :
	function emotions_lite_post_title() {  	
	$this_page_id = get_the_ID(); 
	$title = the_title( '', '', false );
		if (empty($title)) {
			echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink()) . '">', _e('Post ID ', 'emotions-lite') . $this_page_id . '</a></h2>';			
		} else {
			echo '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', the_title(),'</a></h2>';	
		}
	}
endif;

 /**
 * Prints HTML with meta information for the current post-date/time and author.
 */
if ( ! function_exists( 'emotions_lite_posted_on' ) ) :

function emotions_lite_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="screen-reader-text updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'emotions-lite' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);
	
	$byline = sprintf(
		esc_html_x( '%s', 'post author', 'emotions-lite' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

// show sticky featured tag	
	if( is_sticky() && is_home() && get_theme_mod( 'show_featured_tag', 1 ) )  :	
	$featured = esc_attr__('Featured', 'emotions-lite' );
		echo '<span class="featured-post">' . $featured . '</span>';
	endif;
	
// show post date	
	if ( esc_attr(get_theme_mod( 'show_postdate', 1 )) ) :
		echo '<span class="posted-on">' . $posted_on . '</span>';
	endif;
	
// show author
	if ( esc_attr(get_theme_mod( 'show_postauthor', 1 )) ) :
		echo '<span class="byline">' . $byline . '</span>';
	endif;
	
// show comments link	
	if ( esc_attr(get_theme_mod( 'show_commentslink', 1 )) ) :
	if ( ! is_singular() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( sprintf( __( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'emotions-lite' ), get_the_title() ) );
		echo '</span>';
	}				
	endif;		
}
endif;

/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
if ( ! function_exists( 'emotions_lite_entry_footer' ) ) :
function emotions_lite_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */		
		$categories_list = get_the_category_list( __( ', ', 'emotions-lite' ) );
		if ( esc_attr(get_theme_mod( 'show_categories', 1 )) ) :
		if ( $categories_list && emotions_lite_categorized_blog() ) {
			printf( '<div class="cat-links">' . __( 'Posted in %1$s', 'emotions-lite' ) . '</div>', $categories_list ); // WPCS: XSS OK.
		}
		endif;
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', __( ', ', 'emotions-lite' ) );
		if ( esc_attr(get_theme_mod( 'show_tags', 1 )) ) :
		if ( $tags_list ) {
			printf( '<div class="tags-links">' . __( 'Tagged %1$s', 'emotions-lite' ) . '</div>', $tags_list ); // WPCS: XSS OK.
		}
		endif;
	}
	
	
	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		comments_popup_link( __( 'Leave a comment', 'emotions-lite' ), __( '1 Comment', 'emotions-lite' ), __( '% Comments', 'emotions-lite' ) );
		echo '</span>';
	}

	if ( esc_attr(get_theme_mod( 'show_edit', 0 )) ) :
	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			__( 'Edit %s', 'emotions-lite' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
	endif;
}
endif;


/**
 * Multi-page navigation.
 */
if ( ! function_exists( 'emotions_lite_multipage_nav' ) ) :
function emotions_lite_multipage_nav() {
	wp_link_pages( array(
		'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'emotions-lite' ) . '</span>',
		'after'       => '</div>',
		'link_before' => '<span>',
		'link_after'  => '</span>',
		'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'emotions-lite' ) . ' </span>%',
		'separator'   => ', ',
	) );
}
endif;

/**
 * Blog pagination when more than one page of post summaries.
 * Add classes to next_posts_link and previous_posts_link
 */
add_filter('next_posts_link_attributes', 'emotions_lite_posts_link_attributes_1');
add_filter('previous_posts_link_attributes', 'emotions_lite_posts_link_attributes_2');

function emotions_lite_posts_link_attributes_1() {
    return 'class="post-nav-older"';
}
function emotions_lite_posts_link_attributes_2() {
    return 'class="post-nav-newer"';
}

// Output the pagination navigation
if ( ! function_exists( 'emotions_lite_blog_pagination' ) ) :
function emotions_lite_blog_pagination() {	
		echo '<div class="pagination clearfix">';
		echo get_next_posts_link( __('Older Posts', 'emotions-lite'));		
		echo get_previous_posts_link( __('Newer Posts', 'emotions-lite'));
		echo '</div>';	
}
endif;


// Output the format gallery pagination 
if ( ! function_exists( 'emotions_lite_gallery_pagination' ) ) :
function emotions_lite_gallery_pagination() {	
		echo '<div class="gallery-pagination clearfix">';
		echo get_next_posts_link( __('Older Posts', 'emotions-lite'));		
		echo get_previous_posts_link( __('Newer Posts', 'emotions-lite'));
		echo '</div>';	
}
endif;


/**
 * Single Post previous or next navigation.
 */
if ( ! function_exists( 'emotions_lite_post_pagination' ) ) :

function emotions_lite_post_pagination() {
	the_post_navigation( array(	
		'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next Post', 'emotions-lite' ) . '</span> ' .
			'<span class="screen-reader-text">' . __( 'Next Post:', 'emotions-lite' ) . '</span> ' .
			'<span class="post-title">%title</span>',
			
		'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous Post', 'emotions-lite' ) . '</span> ' .
			'<span class="screen-reader-text">' . __( 'Previous Post:', 'emotions-lite' ) . '</span> ' .
			'<span class="post-title">%title</span>',
	) );
}
endif;


/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function emotions_lite_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'emotions_lite_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'emotions_lite_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so emotions_lite_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so emotions_lite_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in emotions_lite_categorized_blog.
 */
function emotions_lite_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'emotions_lite_categories' );
}
add_action( 'edit_category', 'emotions_lite_category_transient_flusher' );
add_action( 'save_post',     'emotions_lite_category_transient_flusher' );
