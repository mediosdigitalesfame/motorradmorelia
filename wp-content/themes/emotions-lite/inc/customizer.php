<?php
/**
 * Emotions Theme Customizer.
 * @package Emotions Lite
 */
 
 
 
function emotions_lite_customizer_registers() {
	
	wp_enqueue_script( 'emotions_lite_customizer_script', get_template_directory_uri() . '/js/emotions-lite-customizer.js', array("jquery"), '1.0', true  );
	wp_localize_script( 'emotions_lite_customizer_script', 'emotionsliteCustomizerObject', array(
		'setup' => __( 'Setup Tutorials', 'emotions-lite' ),
		'support' => __( 'Theme Support', 'emotions-lite' ),
		'review' => __( 'Please Rate Emotions Lite', 'emotions-lite' ),		
		'pro' => __( 'Get the Pro Version', 'emotions-lite' ),
	) );
}
add_action( 'customize_controls_enqueue_scripts', 'emotions_lite_customizer_registers' );
  
// Add postMessage support for site title and description for the Theme Customizer.
function emotions_lite_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_section( 'background_image' )->title = __( 'Body Background Image', 'emotions-lite' );
	
	// lets move the background colour to the background image section
	$wp_customize->get_control( 'background_color' )->section = 'background_image';	
		
		
/*
 * Begin theme options
 * Add more options to the Site Identity section
 */ 
	// Setting group to show the site title  
  	$wp_customize->add_setting( 'show_site_title',  array(
		'default' => 1,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox'
   	 ) );  
 	 $wp_customize->add_control( 'show_site_title', array(
		'type'     => 'checkbox',
		'priority' => 1,
		'label'    => __( 'Show Site Title', 'emotions-lite' ),
		'section'  => 'title_tagline',
 	 ) );

	// Setting group to show the tagline  
	 $wp_customize->add_setting( 'show_description', array(
		'default' => 1,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox'
	  ) );  
	$wp_customize->add_control( 'show_description', array(
		'type'     => 'checkbox',
		'priority' => 2,
		'label'    => __( 'Show Site Description', 'emotions-lite' ),
		'section'  => 'title_tagline',
	) );
	
	
	// Setting group to show the tagline  
	 $wp_customize->add_setting( 'logo_spacing', array(
		'default' => '0 6px 0 0',
		'sanitize_callback' => 'sanitize_text_field'
	  ) );  
	$wp_customize->add_control( 'logo_spacing', array(
		'type'     => 'text',
		'priority' => 4,
		'label'    => __( 'Logo Spacing', 'emotions-lite' ),
		'description' => __( 'When using just a logo, you can adjust the positioning (spacing) for your logo. The default setting is 0 6px 0 0 which means 0 pixels on the top, 6px on the right, 0 pixels on the bottom, and 0 pixels on the left...in that sequence.', 'emotions-lite' ),
		'section'  => 'title_tagline',
	) );	
	
	
/*
 * Create a new customizer section
 * Name: Site Options
 */    
	$wp_customize->add_section( 'site_options', array(
		'title' => __( 'Site Options', 'emotions-lite' ),
		'priority'       => 30,
	) ); 

// Setting group for header style  
	$wp_customize->add_setting( 'header_style', array(
		'default' => 'header1',
		'sanitize_callback' => 'emotions_lite_sanitize_headerstyle',
	) );  
	$wp_customize->add_control( 'header_style', array(
		  'type' => 'radio',
		  'label' => __( 'Header Style', 'emotions-lite' ),
		  'section' => 'site_options',
		  'priority' => 1,
		  'choices' => array(	
				'header1' => __( 'Header Style 1', 'emotions-lite' ),	 
				'header2' => __( 'Header Style 2', 'emotions-lite' ), 
				'header3' => __( 'Header Style 3', 'emotions-lite' ),
		) ) );	

// Setting group for menu top margin	
    $wp_customize->add_setting( 'menu_top_margin',  array(
            'sanitize_callback' => 'absint',
            'default'           => '0',
        ) );
    $wp_customize->add_control( 'menu_top_margin', array(
        'type'        => 'number',
        'priority'    => 2,
        'section'     => 'site_options',
		'label'    => __( 'Main Menu Top Margin', 'emotions-lite' ),
		'description' => __( 'This will adjust your main menu top margin spacing in pixels if you need it to be middle aligned to your logo or site title.', 'emotions-lite' ),
        'input_attrs' => array(
            'min'   => 0,
            'max'   => 100,
            'step'  => 1,
        ),
    ) );	
	
// Setting group for boxed styles
	$wp_customize->add_setting( 'boxed_style', array(
		'default' => 'fullwidth',
		'sanitize_callback' => 'emotions_lite_sanitize_boxedstyle',
	) );  
	$wp_customize->add_control( 'boxed_style', array(
		  'type' => 'radio',
		  'label' => __( 'Boxed Style', 'emotions-lite' ),
		'description' => __( 'This will create a centered boxed layout of your page content for larger screens that can accommodate high resolutions and will show more of the page background.', 'emotions-lite' ),
		  'section' => 'site_options',
		  'priority' => 3,
		  'choices' => array(	
				'fullwidth' => __( 'Full Width', 'emotions-lite' ),	 
				'width1800' => __( 'Boxed Width 1800px', 'emotions-lite' ), 
				'width1600' => __( 'Boxed Width 1600px', 'emotions-lite' ),
				'width1400' => __( 'Boxed Width 1400px', 'emotions-lite' ),
				'width1300' => __( 'Boxed Width 1300px', 'emotions-lite' ),
		) ) );	
	
// Setting group to wrap elements in block
	$wp_customize->add_setting( 'content_block',	array(
 		'default' => 0,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
	) );  
	$wp_customize->add_control( 'content_block', array(
		'type'     => 'checkbox',
		'priority' => 4,
		'label'    => __( 'Enable Content Blocks', 'emotions-lite' ),
		'description' => __( 'This setting wraps posts, pages, and sidebar widgets in white background blocks, but you can also change the background colour.', 'emotions-lite' ),
		'section'  => 'site_options',
  	) );

// Setting group to enable animation
	$wp_customize->add_setting( 'load_animation',	array(
 		'default' => 1,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
	) );  
	$wp_customize->add_control( 'load_animation', array(
		'type'     => 'checkbox',
		'priority' => 5,
		'label'    => __( 'Load Animation Styles', 'emotions-lite' ),
		'description' => __( 'Load animation styles if not you are not using the Animate It plugin.', 'emotions-lite' ),
		'section'  => 'site_options',
  	) );
	
// Setting group to enable font awesome 
	$wp_customize->add_setting( 'load_fontawesome',	array(
 		'default' => 1,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
	) );  
	$wp_customize->add_control( 'load_fontawesome', array(
		'type'     => 'checkbox',
		'priority' => 6,
		'label'    => __( 'Load Font Awesome', 'emotions-lite' ),
		'description' => __( 'Load Font Awesome if not you are not using a plugin for it.', 'emotions-lite' ),
		'section'  => 'site_options',
  	) );	
	
// Setting group to enable bootstrap
	$wp_customize->add_setting( 'load_foundation',	array(
		'default' => 1,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
	) );  
	$wp_customize->add_control( 'load_foundation', array(
		'type'     => 'checkbox',
		'priority' => 7,
		'label'    => __( 'Load Foundation CSS', 'emotions-lite' ),
		'description' => __( 'Load the Foundation grid layout and some limited CSS elements if nothing else is loading it for you.', 'emotions-lite' ),
		'section'  => 'site_options',
	) );
	
// Setting group to show the edit links 
	  $wp_customize->add_setting( 'show_edit',  array(
		  'default' => 0,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_edit', array(
		'type'     => 'checkbox',
		'priority' => 8,
		'label'    => __( 'Show Edit Link', 'emotions-lite' ),
		'description' => __( 'Show the Edit Link on posts and pages.', 'emotions-lite' ),
		'section'  => 'site_options',
	  ) );	
	  
// Setting group for a Copyright
	$wp_customize->add_setting( 'copyright', array(
		'default'        => esc_html__( 'Your Name', 'emotions-lite' ),
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'copyright', array(
		'settings' => 'copyright',
		'label'    => esc_html__( 'Your Copyright Name', 'emotions-lite' ),
		'section'  => 'site_options',		
		'type'     => 'text',
		'priority' => 10,
	) );  


 /*
 * Create a new customizer section
 * Name: Blog Options
 */    
	$wp_customize->add_section( 'blog_options', array(
		'title' => __( 'Blog Options', 'emotions-lite' ),
		'priority'       => 35,
	) ); 

	// Setting group for blog layout  
	$wp_customize->add_setting( 'blog_style', array(
		'default' => 'blog1',
		'sanitize_callback' => 'emotions_lite_sanitize_blogstyle',
	) );  
	$wp_customize->add_control( 'blog_style', array(
		  'type' => 'radio',
		  'label' => __( 'Blog Style', 'emotions-lite' ),
		  'section' => 'blog_options',
		  'priority' => 1,
		  'choices' => array(	
				'blog1' => __( 'Standard with Right Sidebar', 'emotions-lite' ),	 
				'blog2' => __( 'Standard with Left Sidebar', 'emotions-lite' ), 
				'blog3' => __( 'Wide with no Sidebars', 'emotions-lite' ),
				'blog4' => __( 'List with no Sidebars', 'emotions-lite' ),
		) ) );

	// Setting group for single layout  
	$wp_customize->add_setting( 'single_style', array(
		'default' => 'single1',
		'sanitize_callback' => 'emotions_lite_sanitize_singlestyle',
	) );  
	$wp_customize->add_control( 'single_style', array(
		  'type' => 'radio',
		  'label' => __( 'Single Full Post Style', 'emotions-lite' ),
		  'section' => 'blog_options',
		  'priority' => 2,
		  'choices' => array(	
				'single1' => __( 'Single with Right Sidebar', 'emotions-lite' ),	 
				'single2' => __( 'Single with Left Sidebar', 'emotions-lite' ), 
				'single3' => __( 'Single with no Sidebars', 'emotions-lite' ),
		) ) );			


	 // Use excerpt 
	  $wp_customize->add_setting( 'use_excerpt',  array(
		  'default' => 0,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'use_excerpt', array(
		'type'     => 'checkbox',
		'priority' => 3,
		'label'    => __( 'Use Blog Excerpts', 'emotions-lite' ),
		'description' => __( 'This lets you switch to using excerpts for your blog summaries when using one of the first two blog styles.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );

	// Setting group for excerpt length	
    $wp_customize->add_setting( 'excerpt_length',  array(
            'sanitize_callback' => 'absint',
            'default'           => '50',
        ) );
    $wp_customize->add_control( 'excerpt_length', array(
        'type'        => 'number',
        'priority'    => 4,
        'section'     => 'blog_options',
		'label'    => __( 'Excerpt Length', 'emotions-lite' ),
		'description' => __( 'This will adjust your post summary excerpt length by increments of 1 word to an excerpt maximum size of 100 words. Default is 50.', 'emotions-lite' ),
        'input_attrs' => array(
            'min'   => 0,
            'max'   => 100,
            'step'  => 1,
        ),
    ) );
		  
	 // Show the post summary meta info 
	  $wp_customize->add_setting( 'show_summary_meta',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_summary_meta', array(
		'type'     => 'checkbox',
		'priority' => 10,
		'label'    => __( 'Show Summary Meta Info', 'emotions-lite' ),
		'description' => __( 'Show the post summary meta info like the date, author, etc.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );	

	 // Show the featured tag
	  $wp_customize->add_setting( 'show_featured_tag',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_featured_tag', array(
		'type'     => 'checkbox',
		'priority' => 11,
		'label'    => __( 'Show Summary Featured Tag', 'emotions-lite' ),
		'description' => __( 'Show the featured (Sticky) tag on post summaries.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );	
	  
	 // Show the post date 
	  $wp_customize->add_setting( 'show_postdate',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_postdate', array(
		'type'     => 'checkbox',
		'priority' => 11,
		'label'    => __( 'Show Summary Post Date', 'emotions-lite' ),
		'description' => __( 'Show the post date on all blog articles.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );	  
	 // Show the post author 
	  $wp_customize->add_setting( 'show_postauthor',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_postauthor', array(
		'type'     => 'checkbox',
		'priority' => 13,
		'label'    => __( 'Show Summary Post Author', 'emotions-lite' ),
		'description' => __( 'Show the post author on all blog articles.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );

	 // Show the post comments count on the summary 
	  $wp_customize->add_setting( 'show_commentslink',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_commentslink', array(
		'type'     => 'checkbox',
		'priority' => 14,
		'label'    => __( 'Show Summary Comments Link', 'emotions-lite' ),
		'description' => __( 'Show the comments link on the post summaries.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );


	 // Show the single categories list
	  $wp_customize->add_setting( 'show_categories',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_categories', array(
		'type'     => 'checkbox',
		'priority' => 15,
		'label'    => __( 'Show Categories', 'emotions-lite' ),
		'description' => __( 'Show the categories list on the full post view.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );

	 // Show the single tags list
	  $wp_customize->add_setting( 'show_tags',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_tags', array(
		'type'     => 'checkbox',
		'priority' => 16,
		'label'    => __( 'Show Tags', 'emotions-lite' ),
		'description' => __( 'Show the tags list on the full post view.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );

	  // Show the single post nav
	  $wp_customize->add_setting( 'show_postnav',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_postnav', array(
		'type'     => 'checkbox',
		'priority' => 17,
		'label'    => __( 'Show Post Navigation', 'emotions-lite' ),
		'description' => __( 'Show the next previous navigation on the full post view.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );
	  
	 // Show the single author bio
	  $wp_customize->add_setting( 'show_authorbio',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_authorbio', array(
		'type'     => 'checkbox',
		'priority' => 18,
		'label'    => __( 'Show Author Bio', 'emotions-lite' ),
		'description' => __( 'Show the author bio information at the bottom of the full post view.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );

	// Setting group to show footer info on single
	  $wp_customize->add_setting( 'show_single_footer',   array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		)
	  );  
	  $wp_customize->add_control( 'show_single_footer', array(
		'type'     => 'checkbox',
		'priority' => 19,
		'label'    => __( 'Show Full Post Footer Information', 'emotions-lite' ),
		'description' => __( 'Show the footer information such as tags, categories, and anything else all at once.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );	
	  
	// Setting group to show featured image on single
	  $wp_customize->add_setting( 'show_single_featured',   array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		)
	  );  
	  $wp_customize->add_control( 'show_single_featured', array(
		'type'     => 'checkbox',
		'priority' => 20,
		'label'    => __( 'Show Full Post Featured Image', 'emotions-lite' ),
		'description' => __( 'Show the Featured Image on the full post view.', 'emotions-lite' ),
		'section'  => 'blog_options',
	  ) );	



/*
 * Add more options to the Background Image section
 * Name: Background Image
 */  
	$wp_customize->add_setting( 'bgsize', array(
		'default' => 'initial',
		'sanitize_callback' => 'emotions_lite_sanitize_bgsize',
	) );  
	$wp_customize->add_control( 'bgsize', array(
		  'type' => 'radio',
		  'label' => __( 'Background Image Size', 'emotions-lite' ),
		  'section' => 'background_image',
		  'priority' => 20,
		  'choices' => array(	
				'initial' => __( 'Initial', 'emotions-lite' ),
				'auto' => __( 'Auto', 'emotions-lite' ),	 
				'cover' => __( 'Cover', 'emotions-lite' ), 
				'contain' => __( 'Contain', 'emotions-lite' ),
		) ) );			
	
		
/*
 * Lets add to the Header Image section
 * Name: Header Images
 */ 		
		
// Setting group to show the edit links 
	  $wp_customize->add_setting( 'show_default_header',  array(
		  'default' => 1,
		  'sanitize_callback' => 'emotions_lite_sanitize_checkbox',
		) );  
	  $wp_customize->add_control( 'show_default_header', array(
		'type'     => 'checkbox',
		'priority' => 9,
		'label'    => __( 'Show Custom Header', 'emotions-lite' ),
		'description' => __( 'Show the WordPress custom header background image when using the Header Style 3 option.', 'emotions-lite' ),
		'section'  => 'header_image',
	  ) );			
		
		
		
		
/*
 * Lets add to the Colour section
 * Name: Colors
 */ 
 
// body background
 	$wp_customize->add_setting( 'body_bg', array(
		'default'        => '#000',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_bg', array(
		'label'   => __( 'Body Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'body_bg',
		'priority' => 1,			
	) ) );		

// content area background
 	$wp_customize->add_setting( 'content_bg', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_bg', array(
		'label'   => __( 'Content Area Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'content_bg',
		'priority' => 2,			
	) ) );

// content area background
 	$wp_customize->add_setting( 'content_block_bg', array(
		'default'        => '#f5f5f5',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_block_bg', array(
		'label'   => __( 'Content Block Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'content_block_bg',
		'priority' => 2,			
	) ) );
	
// content area text
 	$wp_customize->add_setting( 'content_text', array(
		'default'        => '#505050',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'content_text', array(
		'label'   => __( 'Content Area Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'content_text',
		'priority' => 2,			
	) ) );
	
// header 1 and 2 background
 	$wp_customize->add_setting( 'header_bg', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header_bg', array(
		'label'   => __( 'Header Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'header_bg',
		'priority' => 3,			
	) ) );	
	
// site title colour
 	$wp_customize->add_setting( 'site_title', array(
		'default'        => '#000',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_title', array(
		'label'   => __( 'Site Title Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'site_title',
		'priority' => 4,			
	) ) );		
	
// site description colour
 	$wp_customize->add_setting( 'site_description', array(
		'default'        => '#828788',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'site_description', array(
		'label'   => __( 'Site Description Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'site_description',
		'priority' => 5,			
	) ) );			
	
// breadcrumbs and social background
 	$wp_customize->add_setting( 'breadcrumbs_social_bg', array(
		'default'        => '#ae988e',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumbs_social_bg', array(
		'label'   => __( 'Breadcrumbs &amp; Social Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'breadcrumbs_social_bg',
		'priority' => 6,			
	) ) );	
	
// breadcrumbs and social text
 	$wp_customize->add_setting( 'breadcrumbs_social_text', array(
		'default'        => '#f5f1e9',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumbs_social_text', array(
		'label'   => __( 'Breadcrumbs &amp; Social Text', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'breadcrumbs_social_text',
		'priority' => 7,			
	) ) );		
	
// bottom sidebar background
 	$wp_customize->add_setting( 'bottom_sidebar_bg', array(
		'default'        => '#6b8887',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_sidebar_bg', array(
		'label'   => __( 'Bottom Sidebar Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_sidebar_bg',
		'priority' => 8,			
	) ) );	
// bottom sidebars
 	$wp_customize->add_setting( 'bottom_sidebar_text', array(
		'default'        => '#e3ede9',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_sidebar_text', array(
		'label'   => __( 'Bottom Sidebar Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_sidebar_text',
		'priority' => 9,			
	) ) );	
	
// bottom sidebar link
 	$wp_customize->add_setting( 'bottom_sidebar_link', array(
		'default'        => '#e3ede9',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_sidebar_link', array(
		'label'   => __( 'Bottom Sidebar Link Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_sidebar_link',
		'priority' => 10,			
	) ) );		
	
// bottom sidebar hover link
 	$wp_customize->add_setting( 'bottom_sidebar_hlink', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_sidebar_hlink', array(
		'label'   => __( 'Bottom Sidebar Link Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_sidebar_hlink',
		'priority' => 11,			
	) ) );		

// bottom sidebar list border
 	$wp_customize->add_setting( 'bottom_list_border', array(
		'default'        => '#819695',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_list_border', array(
		'label'   => __( 'Bottom Sidebar List Border Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_list_border',
		'priority' => 11,			
	) ) );
	
// footer background
 	$wp_customize->add_setting( 'footer_bg', array(
		'default'        => '#2a2a2a',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_bg', array(
		'label'   => __( 'Footer Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'footer_bg',
		'priority' => 12,			
	) ) );	
	
// footer text
 	$wp_customize->add_setting( 'footer_text', array(
		'default'        => '#d2cece',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_text', array(
		'label'   => __( 'Footer Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'footer_text',
		'priority' => 13,			
	) ) );		
	
// footer link
 	$wp_customize->add_setting( 'footer_link', array(
		'default'        => '#d2cece',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_link', array(
		'label'   => __( 'Footer Link Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'footer_link',
		'priority' => 14,			
	) ) );		
	
// footer link
 	$wp_customize->add_setting( 'footer_hlink', array(
		'default'        => '#abb8b8',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'footer_hlink', array(
		'label'   => __( 'Footer Link Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'footer_hlink',
		'priority' => 15,			
	) ) );	
	

// links
 	$wp_customize->add_setting( 'links', array(
		'default'        => '#79a2a1',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'links', array(
		'label'   => __( 'Main Content Link Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'links',
		'priority' => 16,			
	) ) );		
	
// links on hover
 	$wp_customize->add_setting( 'links_hover', array(
		'default'        => '#333',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'links_hover', array(
		'label'   => __( 'Main Content Link Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'links_hover',
		'priority' => 17,			
	) ) );	
	
// heading colour
 	$wp_customize->add_setting( 'heading_colour', array(
		'default'        => '#0a0a0a',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'heading_colour', array(
		'label'   => __( 'Headings Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'heading_colour',
		'priority' => 18,			
	) ) );	
	
// widget title colour
 	$wp_customize->add_setting( 'widgettitle_colour', array(
		'default'        => '#0a0a0a',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'widgettitle_colour', array(
		'label'   => __( 'Widget Title Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'widgettitle_colour',
		'priority' => 18,			
	) ) );	

// bottom widget title colour
 	$wp_customize->add_setting( 'bottom_widget_title_colour', array(
		'default'        => '#e3ede9',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bottom_widget_title_colour', array(
		'label'   => __( 'Bottom Widget Title Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'bottom_widget_title_colour',
		'priority' => 18,			
	) ) );
	
// button colour
 	$wp_customize->add_setting( 'button_colour', array(
		'default'        => '#84a8a7',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_colour', array(
		'label'   => __( 'Button Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'button_colour',
		'priority' => 19,			
	) ) );			
	
// button text colour
 	$wp_customize->add_setting( 'button_text_colour', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_text_colour', array(
		'label'   => __( 'Button Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'button_text_colour',
		'priority' => 20,			
	) ) );	
	
// button hover colour
 	$wp_customize->add_setting( 'button_hover_colour', array(
		'default'        => '#6b8887',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_hover_colour', array(
		'label'   => __( 'Button Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'button_hover_colour',
		'priority' => 21,			
	) ) );		
	
// button text hover colour
 	$wp_customize->add_setting( 'button_text_hover_colour', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'button_text_hover_colour', array(
		'label'   => __( 'Button Hover Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'button_text_hover_colour',
		'priority' => 22,			
	) ) );		

// banner caption button border and text
 	$wp_customize->add_setting( 'banner_caption_button', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_caption_button', array(
		'label'   => __( 'Banner Caption Button Border and Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'banner_caption_button',
		'priority' => 23,			
	) ) );	
	
// banner caption button on hover
 	$wp_customize->add_setting( 'banner_caption_hbutton', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_caption_hbutton', array(
		'label'   => __( 'Banner Caption Button Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'banner_caption_hbutton',
		'priority' => 23,			
	) ) );		

// banner caption button text on hover
 	$wp_customize->add_setting( 'banner_caption_htext', array(
		'default'        => '#000',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'banner_caption_htext', array(
		'label'   => __( 'Banner Caption Button Text Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'banner_caption_htext',
		'priority' => 24,			
	) ) );		
	


// mobile menu button background
 	$wp_customize->add_setting( 'mobile_button_bg', array(
		'default'        => '#84a8a7',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_button_bg', array(
		'label'   => __( 'Mobile Menu Button Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_button_bg',
		'priority' => 60,			
	) ) ); 

// mobile menu button background hover
 	$wp_customize->add_setting( 'mobile_button_hbg', array(
		'default'        => '#333',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_button_hbg', array(
		'label'   => __( 'Mobile Menu Button Hover Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_button_hbg',
		'priority' => 61,			
	) ) ); 	
// mobile menu button label
 	$wp_customize->add_setting( 'mobile_button_label', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_button_label', array(
		'label'   => __( 'Mobile Menu Button Label', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_button_label',
		'priority' => 62,			
	) ) );	
// mobile menu button label hover
 	$wp_customize->add_setting( 'mobile_button_hlabel', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_button_hlabel', array(
		'label'   => __( 'Mobile Menu Button Hover Label', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_button_hlabel',
		'priority' => 63,			
	) ) );	

// mobile menu background
 	$wp_customize->add_setting( 'mobile_menu_bg', array(
		'default'        => '#333333',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_bg', array(
		'label'   => __( 'Mobile Menu Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_menu_bg',
		'priority' => 64,			
	) ) );

// mobile menu links
 	$wp_customize->add_setting( 'mobile_menu_links', array(
		'default'        => '#ffffff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_links', array(
		'label'   => __( 'Mobile Menu Links', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_menu_links',
		'priority' => 65,			
	) ) );

// mobile menu hover links
 	$wp_customize->add_setting( 'mobile_menu_hover_links', array(
		'default'        => '#b9a79f',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_hover_links', array(
		'label'   => __( 'Mobile Menu Hover &amp; Active Links', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_menu_hover_links',
		'priority' => 66,			
	) ) );

// mobile menu item borders
 	$wp_customize->add_setting( 'mobile_menu_borders', array(
		'default'        => '#4d4d4d',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'mobile_menu_borders', array(
		'label'   => __( 'Mobile Menu Item Borders', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'mobile_menu_borders',
		'priority' => 67,			
	) ) );
	
// main menu links
 	$wp_customize->add_setting( 'main_menu_links', array(
		'default'        => '#222',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_menu_links', array(
		'label'   => __( 'Main Menu Links', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'main_menu_links',
		'priority' => 70,			
	) ) );	
// main menu hover active links
 	$wp_customize->add_setting( 'main_menu_hover_links', array(
		'default'        => '#b1988d',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'main_menu_hover_links', array(
		'label'   => __( 'Main Menu Hover &amp; Active Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'main_menu_hover_links',
		'priority' => 71,			
	) ) );	
	
// main submenu background
 	$wp_customize->add_setting( 'submenu_background', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_background', array(
		'label'   => __( 'Submenu Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'submenu_background',
		'priority' => 72,			
	) ) );		

// main submenu bottom border
 	$wp_customize->add_setting( 'submenu_border', array(
		'default'        => '#ae988e',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_border', array(
		'label'   => __( 'Submenu Bottom Border Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'submenu_border',
		'priority' => 73,			
	) ) );	
	
// main submenu links
 	$wp_customize->add_setting( 'submenu_links', array(
		'default'        => '#222',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_links', array(
		'label'   => __( 'Submenu Link Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'submenu_links',
		'priority' => 74,			
	) ) );	
	
// main submenu active hover links
 	$wp_customize->add_setting( 'submenu_hover_links', array(
		'default'        => '#b1988d',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_hover_links', array(
		'label'   => __( 'Submenu Hover &amp; Active Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'submenu_hover_links',
		'priority' => 75,			
	) ) );	


// header style 3 menu background
 	$wp_customize->add_setting( 'header3_menu_bg', array(
		'default'        => '',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'header3_menu_bg', array(
		'label'   => __( 'Header 3 Menu Background Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'header3_menu_bg',
		'priority' => 76,			
	) ) );	
	
// social icons
 	$wp_customize->add_setting( 'social_icons', array(
		'default'        => '#f5f1e9',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'social_icons', array(
		'label'   => __( 'Social Icon Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'social_icons',
		'priority' => 76,			
	) ) );		
	
// social icon hover
 	$wp_customize->add_setting( 'social_icons_hover', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'social_icons_hover', array(
		'label'   => __( 'Social Icon Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'social_icons_hover',
		'priority' => 77,			
	) ) );		
	
// single post nav background
 	$wp_customize->add_setting( 'single_post_nav_bg', array(
		'default'        => '#ae988e',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'single_post_nav_bg', array(
		'label'   => __( 'Full Post Navigation Background', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'single_post_nav_bg',
		'priority' => 78,			
	) ) );		
	
// single post nav text
 	$wp_customize->add_setting( 'single_post_nav_text', array(
		'default'        => '#fff',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'single_post_nav_text', array(
		'label'   => __( 'Full Post Navigation Text Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'single_post_nav_text',
		'priority' => 79,			
	) ) );	

// single post nav text on hover
 	$wp_customize->add_setting( 'single_post_nav_htext', array(
		'default'        => '#efe2cf',
		'sanitize_callback' => 'sanitize_hex_color',
	) );	
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'single_post_nav_htext', array(
		'label'   => __( 'Full Post Navigation Text Hover Colour', 'emotions-lite' ),
		'section' => 'colors',
		'settings'   => 'single_post_nav_htext',
		'priority' => 80,			
	) ) );	
	

/*
 * Typography Options
 */  
  $wp_customize->add_section( 'typography_options', array(
      'title' => __( 'Typography Options', 'emotions-lite' ),
	  'description' => __('This theme is setup to use Google Fonts and the Merriweather font is the default font for headings. You can use these typography settings or use a font plugin for more flexibility.', 'emotions-lite'),
	  'priority' => 83,
    )  ); 

	
// Setting group to show the site title  
  	$wp_customize->add_setting( 'load_cyrillic_subset',  array(
		'default' => 0,
		'sanitize_callback' => 'emotions_lite_sanitize_checkbox'
   	 ) );  
 	 $wp_customize->add_control( 'load_cyrillic_subset', array(
		'type'     => 'checkbox',
		'section'  => 'typography_options',
		'priority' => 1,
		'label'    => __( 'Load Cyrillic Font Subsets', 'emotions-lite' ),
		'description' => __( 'If you need the Cyrillic font subsets loaded for the included Google Fonts of Merriweather and your custom fonts, then check this box.', 'emotions-lite' ),
 	 ) );	
 
// Setting group for the main body font
	$wp_customize->add_setting( 'second_google_font', array(
		'default' => '',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'second_google_font', array(
		'settings' => 'second_google_font',
		'label'    => __( 'Add a Second Google Font', 'emotions-lite' ),
		'description' => __( 'This will add a second Google font to your website.', 'emotions-lite' ),
		'section'  => 'typography_options',		
		'type'     => 'text',
		'priority' => 3,
	) );

// Setting group for the heading font
	$wp_customize->add_setting( 'third_google_font', array(
		'default' => '',
		'sanitize_callback' => 'sanitize_text_field',
	) );
	$wp_customize->add_control( 'third_google_font', array(
		'settings' => 'third_google_font',
		'label'    => __( 'Add a Third Google Font', 'emotions-lite' ),
		'description' => __( 'This will add a third Google Font to your website.', 'emotions-lite' ),
		'section'  => 'typography_options',		
		'type'     => 'text',
		'priority' => 4,
	) );
	
	
	
	
	
	
}
add_action( 'customize_register', 'emotions_lite_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function emotions_lite_customize_preview_js() {
	wp_enqueue_script( 'emotions_lite_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'emotions_lite_customize_preview_js' );


/******************************* SANITIZATION ********************************
Remember to sanitize any additional theme settings you add to the customizer.
*********************************************************************************/


// adds sanitization callback function : checkbox
if ( ! function_exists( 'emotions_lite_sanitize_checkbox' ) ) :
	function emotions_lite_sanitize_checkbox( $input ) {
		if ( $input == 1 ) {
			return 1;
		} else {
			return '';
		}
	}	 
endif;

// adds sanitization callback function for background image size
if ( ! function_exists( 'emotions_lite_sanitize_bgsize' ) ) :
  function emotions_lite_sanitize_bgsize( $value ) {
    $bgsize = array( 'initial','auto', 'cover', 'contain' );
    if ( ! in_array( $value, $bgsize ) ) {
      $value = 'initial';
    }
    return $value;
  }
endif;

// adds sanitization callback function for header style
if ( ! function_exists( 'emotions_lite_sanitize_headerstyle' ) ) :
  function emotions_lite_sanitize_headerstyle( $value ) {
    $header_style = array( 'header1', 'header2', 'header3' );
    if ( ! in_array( $value, $header_style ) ) {
      $value = 'header1';
    }
    return $value;
  }
endif;

// adds sanitization callback function for boxed style
if ( ! function_exists( 'emotions_lite_sanitize_boxedstyle' ) ) :
  function emotions_lite_sanitize_boxedstyle( $value ) {
    $boxed_style = array( 'fullwidth', 'width1800', 'width1600', 'width1400', 'width1300' );
    if ( ! in_array( $value, $boxed_style ) ) {
      $value = 'fullwidth';
    }
    return $value;
  }
endif;
// adds sanitization callback function for blog style
if ( ! function_exists( 'emotions_lite_sanitize_blogstyle' ) ) :
  function emotions_lite_sanitize_blogstyle( $value ) {
    $blog_style = array( 'blog1', 'blog2', 'blog3','blog4' );
    if ( ! in_array( $value, $blog_style ) ) {
      $value = 'blog1';
    }
    return $value;
  }
endif;

// adds sanitization callback function for single style
if ( ! function_exists( 'emotions_lite_sanitize_singlestyle' ) ) :
  function emotions_lite_sanitize_singlestyle( $value ) {
    $single_style = array( 'single1', 'single2', 'single3' );
    if ( ! in_array( $value, $single_style ) ) {
      $value = 'single1';
    }
    return $value;
  }
endif;




