<?php
/**
 * Add inline styles to the head area
 * These styles represents options from the customizer
 * @package Emotions Lite
 */
 
 // Dynamic styles
function emotions_lite_inline_styles($custom) {
	
// BEGIN CUSTOM CSS


// body and text
	$body_bg = get_theme_mod( 'body_bg', '#000' );
	$content_text = get_theme_mod( 'content_text', '#505050' );
	$content_bg = get_theme_mod( 'content_bg', '#fff' );
	$bgsize = get_theme_mod( 'bgsize', 'initial' );
	$links = get_theme_mod( 'links', '#79a2a1' );
	$links_hover = get_theme_mod( 'links_hover', '#333' );
	$heading_colour = get_theme_mod( 'heading_colour', '#0a0a0a' );
	$widgettitle_colour = get_theme_mod( 'widgettitle_colour', '#0a0a0a' );
	$bottom_widget_title_colour = get_theme_mod( 'bottom_widget_title_colour', '#e3ede9' );
	$custom .= "body {background-color:" . esc_attr($body_bg) . "; }
	body.custom-background {background-size:" . esc_attr($bgsize) . "; }
	#content {background-color:" . esc_attr($content_bg) . "; color:" . esc_attr($content_text) . "; }
	a, a:visited {color:" . esc_attr($links) . "; }
	a:hover {color:" . esc_attr($links_hover) . "; }
	h1, h2, h3, h4, h5, h6, .entry-title a, .entry-title a:visited, .entry-title a:hover {color:" . esc_attr($heading_colour) . "; }
	.widget-title {color:" . esc_attr($widgettitle_colour) . "; }
	#sidebar-bottom-group .widget-title {color:" . esc_attr($bottom_widget_title_colour) . "; }"."\n";

// page boxed margins	
		$boxed_margins = get_theme_mod( 'boxed_margins', '0' );
		$custom .= "#page {margin:" . esc_attr($boxed_margins) . "px auto; }"."\n";
	
// headers
	$header_bg = get_theme_mod( 'header_bg', '#ffffff' );
	$site_title = get_theme_mod( 'site_title', '#000' );
	$site_description = get_theme_mod( 'site_description', '#828788' );
	$header3_overlay = get_theme_mod( 'header3_overlay', '0.2' );
	$custom .= "#site-header1, #site-header2, #site-header3 {background-color:" . esc_attr($header_bg) . "; }
	#site-title a, #site_title a:visited, #site_title a:hover {color:" . esc_attr($site_title) . "; }
	#site-header3 #site-description:before, #site-header3 #site-description:after {background-color:" . esc_attr($site_description) . "; }	
	#site-description {color:" . esc_attr($site_description) . "; }"."\n";		

	// wp custom header
	$header_text_colour = get_theme_mod( 'header_text_colour', '#fff)' );
	$header_subtext_colour = get_theme_mod( 'header_subtext_colour', '#fff)' );
	$header_button_border = get_theme_mod( 'header_button_border', '#fcd088)' );
	$header_button_hover_bg = get_theme_mod( 'header_button_hover_bg', '#fcd088)' );
	$header_button_hover_text = get_theme_mod( 'header_button_hover_text', '#333' );
	$custom .= ".header-text { color: " . esc_attr($header_text_colour) . "}
	.header-subtext { color: " . esc_attr($header_subtext_colour) . "}
	.header-button { border-color: " . esc_attr($header_button_border) . "; color: " . esc_attr($header_button_border) . "}
	.header-button:hover { background-color: " . esc_attr($header_button_hover_bg) . "; color: " . esc_attr($header_button_hover_text) . "}	"."\n";	
	
	
// breadcrumbs and social area
	$breadcrumbs_social_bg = get_theme_mod( 'breadcrumbs_social_bg', '#ae988e' );
	$breadcrumbs_social_text = get_theme_mod( 'breadcrumbs_social_text', '#f5f1e9' );
	$custom .= "#social-breadcrumbs-wrapper, .search-box {background-color:" . esc_attr($breadcrumbs_social_bg) . "; color:" . esc_attr($breadcrumbs_social_text) . "  }
	#breadcrumbs-wrapper a, .social-navigation a, .search-toggle:before {color:" . esc_attr($breadcrumbs_social_text) . "  }"."\n";		
		
// main menu
	$mobile_button_bg = get_theme_mod( 'mobile_button_bg', '#84a8a7' );
	$mobile_button_hbg = get_theme_mod( 'mobile_button_hbg', '#333' );
	$mobile_button_label = get_theme_mod( 'mobile_button_label', '#fff' );
	$mobile_button_hlabel = get_theme_mod( 'mobile_button_hlabel', '#fff' );
	$mobile_menu_bg = get_theme_mod( 'mobile_menu_bg', '#333333' );
	$mobile_menu_links = get_theme_mod( 'mobile_menu_links', '#ffffff' );
	$mobile_menu_hover_links = get_theme_mod( 'mobile_menu_hover_links', '#b9a79f' );	
	$mobile_menu_borders = get_theme_mod( 'mobile_menu_borders', '#4d4d4d' );
	$main_menu_links = get_theme_mod( 'main_menu_links', '#222' );
	$main_menu_hover_links = get_theme_mod( 'main_menu_hover_links', '#b1988d' );
	$submenu_border = get_theme_mod( 'submenu_border', '#ae988e' );
	$submenu_background = get_theme_mod( 'submenu_background', '#fff' );
	$submenu_links = get_theme_mod( 'submenu_links', '#222' );
	$submenu_hover_links = get_theme_mod( 'submenu_hover_links', '#b1988d' );			
	$header3_menu_bg = get_theme_mod( 'header3_menu_bg', '' );	
	$custom .= ".menu-toggle {background-color:" . esc_attr($mobile_button_bg) . "; color:" . esc_attr($mobile_button_label) . "  }
	.menu-toggle:hover {background-color:" . esc_attr($mobile_button_hbg) . "; color:" . esc_attr($mobile_button_hlabel) . "  }	
	.main-navigation.toggled-on a, 
	.main-navigation.toggled-on li.home a {background-color:" . esc_attr($mobile_menu_bg) . "; color:" . esc_attr($mobile_menu_links) . ";  }
	.toggled-on li {border-color:" . esc_attr($mobile_menu_borders) . "  }
	.main-navigation.toggled-on ul.sub-menu a {color:" . esc_attr($mobile_menu_links) . "; }
		
	.main-navigation.toggled-on li.home a:hover,
	.main-navigation.toggled-on a:hover,	
	.main-navigation.toggled-on .current-menu-item  a,
	.main-navigation.toggled-on .current-menu-ancestor > a,
	.main-navigation.toggled-on ul.sub-menu a:hover,
	.main-navigation.toggled-on ul ul .current-menu-item > a,
	.main-navigation.toggled-on ul ul .current-menu-ancestor > a {color:" . esc_attr($mobile_menu_hover_links) . "; }
	
	.main-navigation li a, .main-navigation li.home a {color:" . esc_attr($main_menu_links) . "  }	
	.main-navigation li.home a:hover,
	.main-navigation a:hover,
	.main-navigation .current-menu-item  a,
	.main-navigation .current-menu-ancestor > a 	{color:" . esc_attr($main_menu_hover_links) . "  }	
	.main-navigation ul ul li a, .main-navigation li li.menu-item-has-children a {color:" . esc_attr($submenu_links) . "  }		
	.main-navigation ul ul a:hover,
	.main-navigation ul ul .current-menu-item > a,
	.main-navigation ul ul .current-menu-ancestor > a {color:" . esc_attr($submenu_hover_links) . "  }		
	.main-navigation ul li:hover > ul,	.main-navigation ul li:focus > ul {background-color:" . esc_attr($submenu_background) . "; border-color:" . esc_attr($submenu_border) . ";  }
	#site-header3 .nav-menu {background-color:" . esc_attr($header3_menu_bg) . "  }"."\n";		

	if( esc_attr(get_theme_mod( 'menu_top_margin', 0 ) ) ) {
		$menu_top_margin = get_theme_mod( 'menu_top_margin', '0' );
		$custom .= "@media (min-width: 992px) {
			#site-navigation {margin-top:" . esc_attr($menu_top_margin) . "px;  }"."\n";
	}
		
// social
	$social_icons = get_theme_mod( 'social_icons', '#f5f1e9' );
	$social_icons_hover = get_theme_mod( 'social_icons_hover', '#fff' );
	$custom .= ".social-navigation a, .search-toggle:before {color:" . esc_attr($social_icons) . "  }
	.social-navigation a:hover, .search-toggle:hover:before {color:" . esc_attr($social_icons_hover) . "  }"."\n";

// single post nav
	$single_post_nav_bg = get_theme_mod( 'single_post_nav_bg', '#ae988e' );
	$single_post_nav_text = get_theme_mod( 'single_post_nav_text', '#fff' );
	$single_post_nav_htext = get_theme_mod( 'single_post_nav_htext', '#efe2cf' );
	$custom .= ".post-navigation {background-color:" . esc_attr($single_post_nav_bg) . "; color:" . esc_attr($single_post_nav_text) . "  }
	.post-navigation a, .post-navigation a:visited {color:" . esc_attr($single_post_nav_text) . "  }
	.post-navigation a:hover {color:" . esc_attr($single_post_nav_htext) . "  }"."\n";		
	
	
// bottom sidebars
	$bottom_sidebar_bg = get_theme_mod( 'bottom_sidebar_bg', '#6b8887' );
	$bottom_sidebar_text = get_theme_mod( 'bottom_sidebar_text', '#e3ede9' );
	$bottom_sidebar_link = get_theme_mod( 'bottom_sidebar_link', '#e3ede9' );
	$bottom_sidebar_hlink = get_theme_mod( 'bottom_sidebar_hlink', '#fff' );
	$bottom_list_border = get_theme_mod( 'bottom_list_border', '#819695' );
	$custom .= "#bottom-wrapper {background-color:" . esc_attr($bottom_sidebar_bg) . "; color:" . esc_attr($bottom_sidebar_text) . "  }
	#bottom-wrapper a, #bottom-wrapper a:visited {color:" . esc_attr($bottom_sidebar_link) . "  }
	#bottom-wrapper li {border-color:" . esc_attr($bottom_list_border) . "  }
	#bottom-wrapper a:hover {color:" . esc_attr($bottom_sidebar_hlink) . "  }"."\n";
	
// footer
	$footer_bg = get_theme_mod( 'footer_bg', '#2a2a2a' );
	$footer_text = get_theme_mod( 'footer_text', '#d2cece' );
	$footer_link = get_theme_mod( 'footer_link', '#d2cece' );
	$footer_hlink = get_theme_mod( 'footer_hlink', '#abb8b8' );
	$custom .= "#site-footer {background-color:" . esc_attr($footer_bg) . "; color:" . esc_attr($footer_text) . "  }
	#site-footer a, #site-footer a:visited {color:" . esc_attr($footer_link) . "  }
	#site-footer a:hover {color:" . esc_attr($footer_hlink) . "  }"."\n";

// content blocks		
	if( esc_attr(get_theme_mod( 'content_block', 0 ) ) ) :
		$content_block_bg = get_theme_mod( 'content_block_bg', '#f5f5f5' );
		$custom .= ".hentry, #left-sidebar .widget, #right-sidebar .widget, #content-top-group .widget, #content-bottom-group .widget, #comments {padding: 20px 40px; background-color:" . esc_attr($content_block_bg) . "  }
		#author-info-box {padding: 20px 0;}
		#comments {margin-top: 50px; }"."\n";
	endif;

// forms
	$button_colour = get_theme_mod( 'button_colour', '#84a8a7' );
	$button_text_colour = get_theme_mod( 'button_text_colour', '#fff' );
	$button_hover_colour = get_theme_mod( 'button_hover_colour', '#6b8887' );
	$button_text_hover_colour = get_theme_mod( 'button_text_hover_colour', '#fff' );
	
	$banner_caption_button = get_theme_mod( 'banner_caption_button', '#fff' );
	$banner_caption_hbutton = get_theme_mod( 'banner_caption_hbutton', '#fff' );
	$banner_caption_htext = get_theme_mod( 'banner_caption_htext', '#000' );
	
	$custom .= ".button {background-color:" . esc_attr($button_colour) . "; color:" . esc_attr($button_text_colour) . "  }
	.button:hover {background-color:" . esc_attr($button_hover_colour) . "; color:" . esc_attr($button_text_hover_colour) . "  }
	.banner-caption .button {border-color:" . esc_attr($banner_caption_button) . "; color:" . esc_attr($banner_caption_button) . "  }
	.banner-caption .button:hover {background-color:" . esc_attr($banner_caption_hbutton) . "; color:" . esc_attr($banner_caption_htext) . "  }"."\n";

			
	// back to top 
		$back_top = get_theme_mod( 'back_top', '#000' );
		$back_top_icon = get_theme_mod( 'back_top_icon', '#fff' );
		$back_top_hover = get_theme_mod( 'back_top_hover', '#304c6f' );
		$custom .= ".back-to-top {background-color:" . esc_attr($back_top) . "; color:" . esc_attr($back_top_icon) . ";}
		.back-to-top:hover {background-color:" . esc_attr($back_top_hover) . ";}"."\n";			
	
	//Output all the styles
	wp_add_inline_style( 'emotions-lite-style', $custom );	
}
add_action( 'wp_enqueue_scripts', 'emotions_lite_inline_styles' );	