<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Emotions Lite
 */


$blogstyle = esc_attr(get_theme_mod( 'blog_style', 'blog1' ));

get_header(); ?>



<div id="primary" class="content-area">
	<div class="column row">
		<main id="main" class="site-main <?php echo $blogstyle ?>" role="main" itemprop="mainContentOfPage">

			<?php if ( get_theme_mod( 'blog_style', 'blog1')) :
				get_template_part( 'template-parts/'. $blogstyle );
			else :
				get_template_part( 'template-parts/content', get_post_format() );						
			endif; ?>					
			
		</main>
	</div>
	
</div>

<?php 
get_footer(); 
