<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Emotions Lite
 */

?>

	</div><!-- #content -->

<div id="bottom-wrapper">
<?php get_sidebar( 'bottom' ); ?>
</div>



<?php // Back to top icon ?>
	<a class="back-to-top"><span class="fa fa-angle-up"></span></a>
	
<footer id="site-footer" class="expanded row" role="contentinfo">


<?php get_sidebar( 'footer' ); ?>

	<nav id="footer-menu" class="column row">
	<?php if ( has_nav_menu( 'footer' ) ) {
		 wp_nav_menu( array( 
				'theme_location' => 'footer', 
				'fallback_cb' => false, 
				'depth' => 1,
				'container' => false, 
			) ); 
		}
	?>
	</nav>
			
	<div id="copyright" class="column row site-info">
	
		<?php esc_html_e('Copyright &copy;', 'emotions-lite'); ?> 
        <?php echo date_i18n( __( 'Y', 'emotions-lite' ) ) ?>
		<?php echo esc_html(get_theme_mod( 'copyright', '' )); ?>. <?php esc_html_e('All rights reserved.', 'emotions-lite'); ?>	
	</div><!-- .site-info -->
	
</footer><!-- #colophon -->

</div><!-- #page -->    
<?php wp_footer(); ?>

</body>
</html>
