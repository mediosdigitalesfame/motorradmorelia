<?php
/**
 * Template Name: Left Column
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package Emotions Lite
 */

get_header(); ?>

<div id="primary" class="content-area">

<?php get_sidebar( 'content-top' ); ?>

	<div class="row"> 
		<div class="medium-8 medium-push-4 columns">
			<main id="main" class="site-main" role="main" itemprop="mainContentOfPage">

				<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'template-parts/content', 'page' ); ?>
				<?php
				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
				comments_template();
				endif;
				?>
				<?php endwhile; // End of the loop. ?>
				
			</main>			
		</div>

		<div class="medium-4 medium-pull-8 columns">        
			<?php get_sidebar( 'left' ); ?>       
		</div>
		
		</div>
	</div>
	
<?php get_sidebar( 'content-bottom' ); ?>
	
</div>

<?php get_footer(); ?>