<?php
/**
 * Template Name: Search
 * @package Emotions Lite
*/

get_header(); ?>


    
<div id="primary" class="content-area">
	<div class="column row">
		<main id="main" class="site-main" role="main" itemprop="mainContentOfPage">               
    
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();
				
				// Include the page content template.
				get_template_part( 'template-parts/content', 'page' );
				
				get_search_form(); 
				
				// End the loop.
				endwhile;
				?>       
  
			</main>
	</div>
</div>


    
<?php get_footer(); ?>    