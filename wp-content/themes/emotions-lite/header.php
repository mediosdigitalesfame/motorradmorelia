<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Emotions Lite
 */
 
$boxed_style = esc_attr(get_theme_mod( 'boxed_style', 'fullwidth' ));

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="<?php echo $boxed_style; ?>">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'emotions-lite' ); ?></a>

		<?php // lets load our header style
		$header_style = esc_attr(get_theme_mod( 'header_style', 'header1' ) ) ;
		get_template_part( 'template-parts/'. $header_style ); 	
		?>

<div id="social-breadcrumbs-wrapper">
<div  class="expanded row">
<div id="breadcrumbs-wrapper" class="small-12 large-6 columns"><?php get_sidebar( 'breadcrumbs' ); ?></div>
<div id="social-wrapper" class="small-12 large-6 columns">

		<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'emotions-lite' ); ?>">
			<div class="search-toggle">
				<a href="#search-container" class="screen-reader-text" aria-expanded="false" aria-controls="search-container"><?php _e( 'Search', 'emotions-lite' ); ?></a>
			</div>	
			<div id="search-container" class="search-box-wrapper hide">
				<div class="search-box clearfix">
					<?php get_search_form(); ?>
				</div>
			</div>
			<?php if ( has_nav_menu( 'social' ) ) :
				wp_nav_menu( array(
					'theme_location' => 'social', 
					'depth' => 1, 
					'container' => false, 
					'menu_class' => 'social-icons', 
					'link_before' => '<span class="screen-reader-text">', 
					'link_after' => '</span>',
				) );						
			endif; ?>

		
		</nav>
</div>
</div>
</div>

	
<div id="content" class="site-content">
