<?php
    
    /**
     *  Appearance / Customize / Layout - config settings
     */

    $default = array(
        'main'          => __( 'Main Sidebar' , 'cronus' ),
        'front-page'    => __( 'Front Page Sidebar' , 'cronus' ),
        'page'          => __( 'Page Sidebar' , 'cronus' ),
        'post'          => __( 'Single Post Sidebar' , 'cronus' )
    );

    $sidebars   = $default;
    $custom     = tempo_validator::get_json( tempo_options::val( 'custom-sidebars' ) );

    if( !empty( $custom ) && is_array( $custom ) )
        $sidebars = array_merge( $default,  $custom );
    

    $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'customize' ), array(
    	'tempo-layout' => array(
    		'title'		=> __( 'Layout' , 'cronus' ),
    		'priority' 	=> 47,
    		'sections'	=> array(
                'tempo-layout-general' => array(
                    'title'             => __( 'General' , 'cronus' ),
                    'description'       => sprintf( __( '%s: On the premium version, the content width for layout with sidebars is 945 pixels.' , 'cronus' ), '<b>' . __( 'IMPORANT', 'cronus' ) . '</b>' ),
                    'fields'            => array(
                        'content-width' => 'unsupport'
                    )
                ),
    			'tempo-layout' => array(
    				'title'             => __( 'Blog & Archives' , 'cronus' ),
                	'description'       => __( 'Default Layout is used for the next templates: Blog, Archives, Categories, Tags, Author and Search Results.' , 'cronus' ),
                	'fields'			=> array(
                		'layout'	=> array(
                			'title'             => __( 'Layout' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'right',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'cronus' ),
                    				'full'  => __( 'Full Width', 'cronus' ),
                    				'right' => __( 'Right Sidebar', 'cronus' )
                				)
                			)
                		),
                		'sidebar'	=> array(
                			'title'             => __( 'Sidebar' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'main',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
    			'tempo-front-page-layout' => array(
    				'title'             => __( 'Front Page' , 'cronus' ),
                	'description'       => __( 'In order to use this option set you need to activate a staic page on Front Page from - "Static Front Page" tab.' , 'cronus' ),
                	//'callback' 			=> 'is_front_page',
                	'fields'			=> array(
                		'front-page-layout' => array(
                			'title'             => __( 'Layout' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'full',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'cronus' ),
                    				'full'  => __( 'Full Width', 'cronus' ),
                    				'right' => __( 'Right Sidebar', 'cronus' )
                				)
                			)
                		),
                		'front-page-sidebar' => array(
                			'title'             => __( 'Sidebar' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'front-page',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
    			'tempo-post-layout' => array(
    				'title'             => __( 'Post' , 'cronus' ),
                	'description'       => __( 'for the each post you can overwrite the Layout options with the custom settings ( on edit page meta box "Layout" ).' , 'cronus' ),
                	'fields'			=> array(
                		'post-layout' => array(
                			'title'             => __( 'Layout' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'right',
                				'options'	=> array(
                					'left'  => __( 'Left Sidebar', 'cronus' ),
                    				'full'  => __( 'Full Width', 'cronus' ),
                    				'right' => __( 'Right Sidebar', 'cronus' )
                				)
                			)
                		),
                		'post-sidebar' => array(
                			'title'             => __( 'Sidebar' , 'cronus' ),
                			'input'				=> array(
                				'type'		=> 'select',
                				'default'	=> 'post',
                				'options'	=> $sidebars
                			)
                		)
                	)
    			),
                'tempo-page-layout' => array(
                    'title'             => __( 'Page' , 'cronus' ),
                    'description'       => __( 'for the each page you can overwrite the Layout options with the custom settings ( on edit page meta box "Layout" ).' , 'cronus' ),
                    'fields'            => array(
                        'page-layout' => array(
                            'title'             => __( 'Layout' , 'cronus' ),
                            'input'             => array(
                                'type'      => 'select',
                                'default'   => 'full',
                                'options'   => array(
                                    'left'  => __( 'Left Sidebar', 'cronus' ),
                                    'full'  => __( 'Full Width', 'cronus' ),
                                    'right' => __( 'Right Sidebar', 'cronus' )
                                )
                            )
                        ),
                        'page-sidebar' => array(
                            'title'             => __( 'Sidebar' , 'cronus' ),
                            'input'             => array(
                                'type'      => 'select',
                                'default'   => 'page',
                                'options'   => $sidebars
                            )
                        )
                    )
                )
    		)
    	)
    ));

    tempo_cfgs::set( 'customize', $cfgs );
?>