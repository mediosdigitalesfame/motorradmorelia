<?php

	/**
	 *	Appearance / Tempo FAQ - config settings
	 */

	$settings = tempo_cfgs::merge( (array)tempo_cfgs::get( 'settings' ), array(
		'appearance' => array(
			'tempo-faq' => array(
			    'sections' 	=> array(
			    	'faq' => array(
			    		'columns' 		=> array(
			    			// COLUMNS
							'right' => array(
								'boxes' 			=> array(
									array(
										'title'		=> __( 'Customize the Theme', 'cronus' ),
										'template'  => 'templates/admin/appearance/faq/customizer'
									)
								)
							)
			    		)
				    ),
					'zeon' => array(
						'title' 		=> __( 'Cronus Premium', 'cronus' ),
						'description'	=> sprintf( __( 'Activate premium features and get extended core functionality without the risk of loosing any data or settings %1s with our %2s that upgrades our Cronus free WordPress theme.', 'cronus' ) , '<br/>', '<a href="' . esc_url( tempo_core::theme( 'premium-faq' ) ) . '" title="' . esc_attr( tempo_core::theme( 'description' ) )  . '" target="_blank">' . __( 'Cronus Premium Solution', 'cronus' ) . '</a>' ),
						'template'  	=> 'templates/admin/appearance/faq/premium'
					),
				)
			)
		)
	));

	if( tempo_core::is_active_premium() )
		unset( $settings['appearance']['tempo-faq']['sections']['zeon'] );

	tempo_cfgs::set( 'settings', $settings );
?>
