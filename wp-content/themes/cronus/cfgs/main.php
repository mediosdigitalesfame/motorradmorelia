<?php

	/**
	 *	General config settings
	 */


	/**
	 *	Theme Config
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'theme' ), array(
		'description' => __( 'Cronus is a free WordPress child Theme that extends the Tempo free WordPress theme.', 'cronus' ),
	));

	tempo_cfgs::set( 'theme', $cfgs );


	/**
	 *	Custom Logo
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-logo' ), array(
        'height'      	=> 70,
        'width'       	=> 235,
        'flex-height' 	=> true,
		'flex-width'  	=> true,
		'header-text'	=> array( 'tempo-site-title', 'tempo-site-description' )
    ));

    tempo_cfgs::set( 'custom-logo', $cfgs );


    /**
     *	Custom Background
     */

    $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-background' ), array(
        'default-color'         => '#ffffff',
        'default-image'         => null,
        'default-attachment'    => 'scroll'
	));

	tempo_cfgs::set( 'custom-background', $cfgs );


	/**
     *	Custom Header
     */

    $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'custom-header' ), array(
        'default-image' => get_stylesheet_directory_uri() . '/media/img/header.jpg',
		'width'         => 2560,
		'height'        => 1440
    ));

    tempo_cfgs::set( 'custom-header', $cfgs );


	/**
	 *	Images Size
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'images-size' ), array(
		'tempo-header' 	=> array(
			'width' 	=> 2560,
			'height'    => 1440,
			'crop' 		=> true
		),
		'tempo-classic' => array(
			'width' 	=> 1140,
			'height'	=> 640,
			'crop' 		=> true
		),
		'tempo-991'		=> array(
			'width' 	=> 991,
			'height'	=> 560,
			'crop' 		=> true
		)
	));

	tempo_cfgs::set( 'images-size', $cfgs );

	/**
	 *	Headers
	 */

	$cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'headers' ), array(
		'mask-color'	=> '#1c2633',
		'mask-transp'	=> 60
	));

	tempo_cfgs::set( 'headers', $cfgs );

	/**
	 *	Asides Columns
	 */

	 $cfgs = tempo_cfgs::merge( (array)tempo_cfgs::get( 'asides' ), array(
 	   'header-front-page' 	=> 3,
	   'header-blog' 		=> 3,
	   'footer-light' 		=> 4,
	   'footer-dark' 		=> 4
    ));

    tempo_cfgs::set( 'aside', $cfgs );
?>
