Version 0.0.19 ( May 17, 2017 )
________________________________________

- Improvements for Responsive Header Image using srcset="..." attribute
- Improvements for Responsive Featured Image using srcset="..." attribute
- fix Headline as Text if Headline URL is null
- fix Description as Text if Description URL is null

Version 0.0.18 ( May 11, 2017 )
________________________________________

- Responsive Header Image using srcset="..." attribute
- Responsive Header Image Scaling and Spacing Improvements

Version 0.0.17 ( April 5, 2017 )
________________________________________

- auto hyphens settings ( general, header, headings and content )

Version 0.0.16 ( March 23, 2017 )
________________________________________

- multiple responsive improvements
- added small php improvements

Version 0.0.15 ( February 6, 2017 )
________________________________________

- a small update to sync with parent theme

Version 0.0.14 ( January 27, 2017 )
________________________________________

- added small admin css improvements
- added small css improvements
- added small php improvements

Version 0.0.13 ( December 27, 2016 )
________________________________________

- added small improvements for Admin Dashboard
- added small css improvements

Version 0.0.12 ( December 20, 2016 )
________________________________________

- added small improvements for Height of the Header Elements

Version 0.0.11 ( December 15, 2016 )
________________________________________

- small css fix for Header Logo
- added min height for Header Image


Version 0.0.9 ( December 8, 2016 )
________________________________________

- added default Header Image
- small css fix for Header
