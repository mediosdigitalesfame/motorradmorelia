<p><?php printf( __( 'The theme is %s but not contain the translated files for each language.', 'cronus' ), '<b>' . __( 'Translation Ready', 'cronus' ) . '</b>' ); ?></p>
<p><?php printf( __( 'So, you can translate the theme in your favorite language. More details about %1s you can find %2s.', 'cronus' ), '<b>' . __( 'Localization', 'cronus' ) . '</b>', '<a href="https://developer.wordpress.org/themes/functionality/localization/#pot-portable-object-template-files" target="_blank">' . __( 'here', 'cronus' ) . '</a>' ); ?></p>

<p><big><strong><?php _e( 'Theme languages details', 'cronus' ); ?></strong></big></p>

<p><?php printf( __( 'After you will unzip the archive %1s you must have folder %2s with theme files. Go to %3s here you can see file %3s ( Portable Object Template ).', 'cronus' ), '<b>cronus.zip</b>', '<b>cronus</b>', '<b>cronus/languages/</b>', '<b>cronus.pot</b>' ); ?></p>
