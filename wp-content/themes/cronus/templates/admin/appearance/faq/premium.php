<a href="<?php echo esc_attr( tempo_core::theme( 'premium-features' ) ); ?>" class="zeon-image" target="_blank" title="<?php echo esc_attr( tempo_core::theme( 'description' ) ); ?>">
	<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/media/admin/img/cronus+zeon.png' ); ?>" alt="<?php echo esc_attr( tempo_core::theme( 'description' ) ); ?>" style="margin: 0px auto; display: block; max-width: 100%;"/>
</a>

<div class="tempo-features-diff-wrapper">
	<table class="tempo-features-diff">
		<tbody>
			<tr>
				<th class="tempo-feature"><?php _e( 'Features', 'cronus' ); ?></th>
				<th class="tempo-free"><?php _e( 'Cronus', 'cronus' ); ?>
					<div>
						<small><?php _e( 'free version', 'cronus' ); ?></small>
					</div>
				</th>
				<th class="tempo-premium"><?php _e( 'Cronus Premium', 'cronus' ); ?>
					<div>
						<a href="<?php echo esc_url( tempo_core::theme( 'premium-features' ) ); ?>" target="_blank" title="<?php echo esc_attr( tempo_core::theme( 'description' ) ); ?>">
							<small><?php _e( 'upgrade to premium', 'cronus' ); ?></small>
						</a>
					</div>
				</th>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Support', 'cronus' ); ?></td>
				<td class="tempo-free">Basic</td>
				<td class="tempo-free">Priority</td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Customizations', 'cronus' ); ?></td>
				<td class="tempo-free">Selective ( at our choice )</td>
				<td class="tempo-free">Basic &amp; Selective ( at our choice )</td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Favicon', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Logo ( sample )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Logo ( icon )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Colors', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Menu Custom Colors', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header for each Post ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header for each Page ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Sample Image ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Hero Image ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Portfolio ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Audio ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Video ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Header type Google Map ( one click setup )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Highlight Search Results', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Responsive layout', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Sidebars &amp; Custom Layouts', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Sidebars builder', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Front Page custom Header Sidebar', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><?php _e( '1 - 4 ( you can customize number of columns )', 'cronus' ); ?></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Blog custom Header Sidebar', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><?php _e( '1 - 4 ( you can customize number of columns )', 'cronus' ); ?></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Footer light Section custom Sidebar', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><?php _e( '1 - 4 ( you can customize number of columns )', 'cronus' ); ?></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Footer dark Section custom Sidebar', 'cronus' ); ?></td>
				<td class="tempo-free"><?php _e( '4 columns', 'cronus' ); ?></td>
				<td class="tempo-free"><?php _e( '1 - 4 ( you can customize number of columns )', 'cronus' ); ?></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Sidebars for each Post', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Sidebars for each Page', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom breadcrumbs settings', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Shortcodes', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Shortcodes Manager', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>


			<tr>
				<td class="tempo-feature"><?php _e( 'Custom Page Template', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Classic Blog View', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free">+ custom page template</td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Grid Blog View ( + custom page template )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Portfolio Blog View ( + custom page template )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom page template filter posts by tags', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom page template filter posts by categories', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>



			<tr>
				<td class="tempo-feature"><?php _e( 'Footer copyright control', 'cronus' ); ?></td>
				<td class="tempo-free"><?php _e( 'PARTIAL', 'cronus' ); ?></td>
				<td class="tempo-free"><?php _e( 'FULL', 'cronus' ); ?></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Footer custom Menu', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Footer social Items', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Custom CSS Options ( special support: IE, IE8, IE9, IE10, IE11 )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Plugin Contact Form 7 ( styled )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
				<td class="tempo-free"><i class="tempo-icon-ok-circle-1"></i></td>
			</tr>

			<tr>
				<td class="tempo-feature"><?php _e( 'Plugin Easy Google Fonts ( support )', 'cronus' ); ?></td>
				<td class="tempo-free"><i class="tempo-icon-cancel-circled-2"></i></td>
				<td class="tempo-free"><?php _e( 'FULL ( ~ 25 Custom Controlls )', 'cronus' ); ?></td>
			</tr>

		</tbody>
	</table>

	<a href="<?php echo esc_url( tempo_core::theme( 'premium-button' ) ); ?>" target="_blank" class="tempo-button tempo-premium-upgrade tempo-submit-options" title="<?php echo esc_attr( tempo_core::theme( 'description' ) ); ?>">
		<i class="tempo-icon-publish"></i> <?php _e( 'Upgrade to Premium', 'cronus' ) ?>
		<small><?php _e( 'compatible with free version', 'cronus' ); ?></small>
	</a>

	<div class="clear clearfix"></div>
</div>
