<p><?php _e( 'This theme comes with a set of options what allow you to customize content, header, layouts, social items and others. You can see next theme options panels if you go to:', 'cronus' ); ?></p>
<p><strong><?php _e( 'Appearance &rsaquo; Customize', 'cronus' ); ?></strong></p>
<p><?php _e( 'Here you can see:', 'cronus' ); ?></p>

<ol>
	<li><?php _e( 'Site Identity', 'cronus' ); ?></li>
	<li><?php _e( 'Colors', 'cronus' ); ?></li>
	<li><?php _e( 'Background Image', 'cronus' ); ?></li>
	<li><?php _e( 'Menu Appearance', 'cronus' ); ?></li>
	<li><?php _e( 'Header Image', 'cronus' ); ?></li>
	<li>
		<?php _e( 'Header Elements', 'cronus' ); ?>

		<ul>
			<li><?php _e( 'General', 'cronus' ) ?></li>
			<li><?php _e( 'Appearance', 'cronus' ) ?></li>
			<li><?php _e( 'Headline', 'cronus' ) ?></li>
			<li><?php _e( 'Description', 'cronus' ) ?></li>
			<li><?php _e( 'First Button', 'cronus' ) ?></li>
			<li><?php _e( 'Second Button', 'cronus' ) ?></li>
		</ul>
	</li>
	<li><?php _e( 'Additional', 'cronus' ); ?></li>
	<li><?php _e( 'Breadcrumbs', 'cronus' ); ?></li>
	<li><?php _e( 'Layout', 'cronus' ); ?></li>
	<li><?php _e( 'Blog', 'cronus' ); ?></li>
	<li><?php _e( 'Single Post', 'cronus' ); ?></li>
	<li><?php _e( 'Social', 'cronus' ); ?></li>
	<li>
		<?php _e( 'Others', 'cronus' ); ?>

		<ul>
			<li><?php _e( 'Copyright', 'cronus' ) ?></li>
			<li><?php _e( 'Custom CSS ( with support for: IE, IE 10, IE 9 and IE 8 )', 'cronus' ) ?></li>
		</ul>

	</li>
	<li><?php _e( 'Menus', 'cronus' ); ?></li>
	<li><?php _e( 'Widgets', 'cronus' ); ?></li>
	<li><?php _e( 'Static Front Page', 'cronus' ); ?></li>
	<li><?php _e( 'Additional CSS', 'cronus' ); ?></li>
</ol>

<p><?php _e( 'All you have to do is play with them and view the live changes. Try and you will discover how easy it is to customize your own style.', 'cronus' ); ?></p>
