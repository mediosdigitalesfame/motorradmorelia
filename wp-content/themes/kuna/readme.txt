== Kuna ==
Contributors: FreeThemes4WP
Tags: one-column, two-columns, right-sidebar, custom-background, custom-menu, featured-images, theme-options, translation-ready
Requires at least: 4.4
Tested up to: 4.7
Version: 1.0.2
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

A simple blogging theme

== Description ==

Kuna is really fast, clean a modern, responsive blogging WordPress theme that has an eye-catching design. Kuna is super fast and SEO friendly WordPress Theme. 

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

= Theme Features Usage =
All available options can be used from Appearance->Customize

== Copyright ==

Kuna WordPress Theme, Copyright 2017 FreeThemes4WP
Kuna is distributed under the terms of the GNU GPL

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== RESOURCES ==

* Image in the screenshot are from http://pixabay.com/:
  - https://pixabay.com/en/tianjin-twilight-city-scenery-2185510/ | License: CC0 Public Domain
* Twitter Bootstrap Framework - http://getbootstrap.com | MIT License
* WordPress nav walker - https://github.com/twittem/wp-bootstrap-navwalker | GPL 2.0
* #unveil.js - https://github.com/luis-almeida/unveil | MIT License
* Other custom js files are our own creation and is licensed under the same license as this theme.

All other resources and theme elements are licensed under the [GNU GPL](http://www.gnu.org/licenses/gpl-3.0.txt), version 3 or later.

Kuna is distributed under the terms of the GNU GPL (http://www.gnu.org/licenses/gpl-3.0.txt), version 3 or later.

== Changelog ==

= Version 1.0.2 =
* Code fixes based on WP requirements

= Version 1.0.1 =
* Small CSS tweaks
* Added support for the header image

= Version 1.0 =
* Initial release